(define-module (themescheme)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build utils)
  #:use-module (guix build haskell-build-system)
  #:use-module (guix build-system haskell)
  #:use-module (gnu packages haskell)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages haskell-web)
  #:use-module (gnu packages haskell-check))

(define-public ghc-string-conv
  (package
    (name "ghc-string-conv")
    (version "0.2.0")
    (source (origin
              (method url-fetch)
              (uri (hackage-uri "string-conv" version))
              (sha256
               (base32
                "15lh7b3jhhv4bwgsswmq447nz4l97gi0hh8ws9njpidi1q0s7kir"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "string-conv")))
    (native-inputs (list ghc-quickcheck-instances ghc-tasty
                         ghc-tasty-quickcheck))
    (home-page "https://github.com/Soostone/string-conv")
    (synopsis "Standardized conversion between string types")
    (description
     "Avoids the need to remember many different functions for converting string
types.  Just use one universal function toS for all monomorphic string
conversions.")
    (license license:bsd-3)))

(define-public ghc-katip
  (package
    (name "ghc-katip")
    (version "0.8.7.4")
    (source (origin
              (method url-fetch)
              (uri (hackage-uri "katip" version))
              (sha256
               (base32
                "0gikcg4cya8gn7cs6n5i3a1xavzzn26y6hwnxng2s362bcscjqjv"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "katip")))
    (inputs (list ghc-aeson
                  ghc-async
                  ghc-auto-update
                  ghc-either
                  ghc-safe-exceptions
                  ghc-hostname
                  ghc-old-locale
                  ghc-string-conv
                  ghc-transformers-compat
                  ghc-unordered-containers
                  ghc-monad-control
                  ghc-transformers-base
                  ghc-resourcet
                  ghc-scientific
                  ghc-microlens
                  ghc-microlens-th
                  ghc-semigroups
                  ghc-unliftio-core))
    (native-inputs (list ghc-tasty
                         ghc-tasty-golden
                         ghc-tasty-hunit
                         ghc-tasty-quickcheck
                         ghc-quickcheck-instances
                         ghc-time-locale-compat
                         ghc-regex-tdfa))
    (home-page "https://github.com/Soostone/katip")
    (synopsis "A structured logging framework.")
    (description
     "Katip is a structured logging framework.  See README.md for more details.")
    (license license:bsd-3)))

(define-public ghc-io-storage
  (package
    (name "ghc-io-storage")
    (version "0.3")
    (source (origin
              (method url-fetch)
              (uri (hackage-uri "io-storage" version))
              (sha256
               (base32
                "1ga9bd7iri6vlsxnjx765yy3bxc4lbz644wyw88yzvpjgz6ga3cs"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "io-storage")))
    (home-page "http://github.com/willdonnelly/io-storage")
    (synopsis "A key-value store in the IO monad.")
    (description
     "This library allows an application to extend the global state hidden inside the
IO monad with semi-arbitrary data.  Data is required to be Typeable'.  The
library provides an essentially unbounded number of key-value stores indexed by
strings, with each key within the stores also being a string.")
    (license license:bsd-3)))

(define-public ghc-dyre
  (let ((commit "6415748bd1e178c08a0150f6721422d87f20b035")
	(revision "1"))
    (package
      (name "ghc-dyre")
      (version (git-version "0.9.1" revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "https://github.com/SkeletonAdventure/dyre")
	       (commit commit)))
	 (file-name (git-file-name name version))
	 (sha256
	  (base32
	   "19njlfjdfdwqbx4p572q13q19363jl61ava8vi4lnzylixli9f9d"))))
      (build-system haskell-build-system)
      (inputs (list ghc-executable-path ghc-io-storage ghc-xdg-basedir))
      (arguments
       (list
	#:tests? #f))
      (home-page "http://github.com/SkeletonAdventure/dyre")
      (synopsis "Dynamic reconfiguration in Haskell")
      (description
       "Dyre implements dynamic reconfiguration facilities after the style of Xmonad.
Dyre aims to be as simple as possible without sacrificing features, and places
an emphasis on simplicity of integration with an application.  A full
introduction with a complete example project can be found in the documentation
for Config.Dyre")
      (license license:bsd-3))))

(define-public ghc-cond
  (package
    (name "ghc-cond")
    (version "0.4.1.1")
    (source (origin
              (method url-fetch)
              (uri (hackage-uri "cond" version))
              (sha256
               (base32
		"12xcjxli1scd4asr4zc77i5q9qka2100gx97hv3vv12l7gj7d703"))))
    (build-system haskell-build-system)
    (properties '((upstream-name . "cond")))
    (home-page "https://github.com/kallisti-dev/cond")
    (synopsis "Basic conditional operators with monadic variants.")
    (description "")
    (license license:bsd-3)))

(define-public themescheme-dev
  (package
    (name "themescheme-dev")
    (version "0")
    (source (local-file "." "themescheme"
			#:select? (git-predicate ".")
			#:recursive? #t))
    (build-system haskell-build-system)
    (inputs (list ghc-lens
		  ghc-cond
		  ghc-katip
		  ghc-dyre
		  ghc-xdg-basedir))
    (home-page "")
    (synopsis "")
    (description "")
    (license license:gpl3)))

themescheme-dev
