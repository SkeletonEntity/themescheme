{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
module ThemeScheme.Config where

import           Control.Lens
import           Control.Monad
import           Control.Monad.Cont
import           Data.Functor
import           Data.Maybe
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as TIO
import           GHC.IO.Exception
import           Katip
import           System.Directory
import           System.FilePath
import           Text.Parsec
import           Text.Parsec.Text
import           Text.Printf
import           ThemeScheme.Color
import           ThemeScheme.Logger
import           ThemeScheme.Palette
import           ThemeScheme.Util

default (T.Text)

-- |Config files will be parsed for text with the 'prefix' before it and the
--'suffix' after it, where the name of the color from the 'Palette' is in the
--middle.
--
--For example, with @ColorInjectorFormat \"{|\" \"|}\" toHex@ The text \"{|red|}\"
--would be changed to "#FF0000" if the 'Palette'\'s red was @RGB 1 0 0@.
--
--It is important to chose a prefix and suffix that do not appear elsewhere in
--the config file.
data ColorInjectorFormat = ColorInjectorFormat
  { _prefix    :: T.Text
  , _suffix    :: T.Text
  , _rgbWriter :: RGB -> T.Text
  }

$(makeLenses ''ColorInjectorFormat)

-- |A single config file to be injected.
--In short, this should point to a config file ending in a \'.theme\' extension.
--The @injectorFormat@ will be used to resolve color references, and the
--injected version of the file will be saved excluding the \'.theme\' extension.
--This should overwrite the config file for the program.
data ConfigInjector = ConfigInjector
  {
    -- |The color injector format used in the .theme file.
    _injectorFormats :: [ColorInjectorFormat]

    -- |The path to the theme config file, formatted as file-name.ext.theme.
    --The themed version of the config file is saved without the .theme, so
    --file-name.ext
  , _configPath     :: FilePath
  }

$(makeLenses ''ConfigInjector)

-- |IO actions that stop and start a program. Look to 'System.Process'
--namely 'callCommand' and 'spawnCommand' for running programs and shell scripts
--through IO.
data ProgramRunner = ProgramRunner
  {
    -- |IO action that starts the program. Fork this action or the program may
    -- block until the execution of this program is complete.
    _startProgram :: FullPalette RGB -> Logger IO ()

    -- |IO action that stops the program.
  , _stopProgram  :: FullPalette RGB -> Logger IO ExitCode
  }

$(makeLenses ''ProgramRunner)

-- |Hooks that run at different points during 'applyProgramConfig'.
data ProgramHooks = ProgramHooks
  {
    -- |This action is preformed before the program is stopped.
    _programPrepareHook    :: FullPalette RGB -> Logger IO ()

    -- |This action is preformed before each theme file is injected.
  , _programPreinjectHook  :: ConfigInjector -> FullPalette RGB -> Logger IO ()

    -- |This action is preformed after each theme file is injected.
  , _programPostinjectHook :: ConfigInjector -> FullPalette RGB -> Logger IO ()

    -- |This action is preformed after the program is started.
  , _programFinalizeHook   :: FullPalette RGB -> Logger IO ()
  }

$(makeLenses ''ProgramHooks)

-- |A convenience for building new 'ProgramHooks'. The 'newProgramHooks'
--preforms @pure ()@ for all hooks. Use 'Control.Lens.Operators' to easily build
--new hooks:
-- >newProgramHooks &~ do
-- >  programPrepareHook .= myPrepareHook
-- >  programFinalizeHook .= myFinalizeHook
newProgramHooks :: ProgramHooks
newProgramHooks = ProgramHooks (\_ -> pure ())
                               (\_ _ -> pure ())
                               (\_ _ -> pure ())
                               (\_ -> pure ())

-- |Defines where and how a 'Palette' will be used to theme a single program.
data ProgramConfig = ProgramConfig
  {
    -- |The name of the program.
    _programName     :: T.Text

    -- |'ConfigInjectors' for the config files of the program.
  , _configInjectors :: [ConfigInjector]

    -- |Set as 'Nothing' if the program does not need to be restarted to apply the
    --theme.
  , _programRunner   :: Maybe ProgramRunner

  , _programHooks    :: ProgramHooks
  }

$(makeLenses ''ProgramConfig)

-- |Hooks that run at different points during 'applySystemConfig'.
data SystemHooks = SystemHooks
  {
    -- |This action is preformed before any 'ProgramConfig's are processed.
    _systemPrepareHook     :: FullPalette RGB -> Logger IO ()

    -- |This action is preformed before applying each 'ProgramConfig'.
  , _systemPreprocessHook  :: FullPalette RGB -> ProgramConfig -> Logger IO ()

    -- |This action is preformed after applying each 'ProgramConfig'.
  , _systemPostprocessHook :: FullPalette RGB -> ProgramConfig -> Logger IO ()

    -- |This action is preformed after all 'ProgramConfig's are processed.
  , _systemFinalizeHook    :: FullPalette RGB -> Logger IO ()
  }

$(makeLenses ''SystemHooks)

-- |A convenience for building 'SystemHooks'. See 'newProgramHooks' for details
--on how to use this function.
newSystemHooks :: SystemHooks
newSystemHooks = SystemHooks (\_ -> pure ())
                             (\_ _ -> pure ())
                             (\_ _ -> pure ())
                             (\_ -> pure ())

-- |Defines where and how a 'Palette' will be applied for an entire system.
data SystemConfig = SystemConfig
  {
    -- |Give your system config set a nice name.
    _systemConfigName :: T.Text

    -- |The config to apply theme to.
  , _programConfigs   :: [ProgramConfig]
  , _systemHooks      :: SystemHooks
  }

$(makeLenses ''SystemConfig)

-- |A convenience function that runs the start scripts for all programs in
--the 'SystemConfig'.
startSystem :: FullPalette RGB -> SystemConfig -> Logger IO ()
startSystem t SystemConfig {..} =
  let starter p = if isJust (p ^. programRunner)
        then do
          $(logTM) InfoS $ ls' $ printf "Starting %s" (p ^. programName)
          p ^. programRunner . singular _Just . startProgram $ t
        else pure ()
  in  mapM_ starter _programConfigs

-- |A convenience function that runs the stop scripts for all programs in
--the 'SystemConfig'.
stopSystem :: FullPalette RGB -> SystemConfig -> Logger IO ()
stopSystem t SystemConfig {..} =
  let stopper p = if isJust (p ^. programRunner)
        then do
          $(logTM) InfoS $ ls' $ printf "Stopping %s" (p ^. programName)
          void $ p ^. programRunner . singular _Just . stopProgram $ t
        else pure ()
  in  mapM_ stopper _programConfigs

-- |A convenience function that runs the stop then start scripts for all
--programs in the 'SystemConfig'.
restartSystem :: FullPalette RGB -> SystemConfig -> Logger IO ()
restartSystem p t = stopSystem p t >> startSystem p t

-- |Preforms 'applyProgramConfig' for each 'ProgramConfig' in '_programConfigs'.
applySystemConfig :: FullPalette RGB -> SystemConfig -> Logger IO ()
applySystemConfig palette (SystemConfig name configs SystemHooks {..}) = do
  $(logTM) InfoS $ ls' $ printf "Applying %s with %s"
                                name
                                (palette ^. paletteName)
  _systemPrepareHook palette
  forM_
    configs
    (\config -> do
      _systemPreprocessHook palette config
      applyProgramConfig palette config
      _systemPostprocessHook palette config
    )
  _systemFinalizeHook palette
  $(logTM) InfoS $ ls' $ printf "Finished applying %s with %s"
                                name
                                (palette ^. paletteName)

-- |Applies of the 'ConfigInjector's in the 'ProgramConfig' and handles starting
--and stopping the program.
applyProgramConfig :: FullPalette RGB -> ProgramConfig -> Logger IO ()
applyProgramConfig palette (ProgramConfig name injectors runner ProgramHooks {..})
  = do
    $(logTM) InfoS $ ls' $ printf "Working on %s" name
    _programPrepareHook palette
    stopped <- stop palette runner
    if stopped
      then do
        forM_
          injectors
          (\injector -> do
            exists <- liftIO $ doesFileExist $ injector ^. configPath
            if exists
              then do
                _programPreinjectHook injector palette
                inject injector
                _programPostinjectHook injector palette
              else $(logTM) ErrorS $ ls' $ printf "File does not exist: %s"
                                                  (injector ^. configPath)
          )
        start palette runner
        _programFinalizeHook palette
      else $(logTM) ErrorS $ ls' $ printf "Skipping %s" name
    $(logTM) InfoS $ ls' $ printf "Finished work on %s" name
 where
  start p (Just r) =
    $(logTM) InfoS (ls' $ printf "Starting %s" name) >> _startProgram r p
  start _ Nothing = pure ()

  stop p (Just r) = do
    $(logTM) InfoS $ ls' $ printf "Stopping " name
    e <- _stopProgram r p
    case e of
      ExitSuccess -> do
        $(logTM) InfoS $ ls' $ printf "Exited %s successfully" name
        pure True
      ExitFailure n -> do
        $(logTM) ErrorS $ ls' $ printf "Failed to stop %s. Exit code: %s"
                                       name
                                       (show n)
        pure False
  stop _ Nothing = pure True

  inject ConfigInjector {..}
    | takeExtension _configPath /= ".theme" = $(logTM) ErrorS $ ls' $ printf
      "File '%s' does not end in '.theme'! Skipping file..."
      _configPath
    | otherwise = do
      $(logTM) InfoS $ ls' $ printf "Applying %s to %s"
                                    (palette ^. paletteName)
                                    _configPath
      configText <- liftIO $ TIO.readFile _configPath
      newConfigText <- foldM 
        (\text cif -> do
            let parsed = injectConfig cif palette text
            case parsed of
              Left err -> do
                $(logTM) ErrorS $ ls' $ printf "Failed to parse %s\n%s"
                  _configPath
                  (show err)
                pure text
              Right t -> pure t 
        )
        configText
        _injectorFormats
      liftIO $ TIO.writeFile (dropExtension _configPath) newConfigText

-- |Parses the file and replaces the color references with the
--referenced 'FullPalette' color converted to text using the 'rgbWriter'.
injectConfig
  :: ColorInjectorFormat
  -> FullPalette RGB
  -> T.Text
  -> Either ParseError T.Text
injectConfig ColorInjectorFormat {..} palette text =
  let
    uprefix = T.unpack _prefix
    usuffix = T.unpack _suffix
    digest  = fullPaletteDigest palette

    parseBlob :: Parser T.Text
    parseBlob = do
      c    <- anyChar
      rest <- manyTill anyChar (try (lookAhead parseRef $> () <|> eof))
      pure $ T.pack $ c : rest

    parseRef :: Parser T.Text
    parseRef = T.pack <$> (string uprefix *> many1 alphaNum <* string usuffix)

    parseBlobThenRef :: Parser (T.Text, Maybe T.Text)
    parseBlobThenRef = do
      blob <- parseBlob
      ref  <- optionMaybe parseRef
      pure (blob, ref)

    parseConfig :: Parser [(T.Text, Maybe T.Text)]
    parseConfig = many parseBlobThenRef

    recompose :: [(T.Text, Maybe T.Text)] -> T.Text
    recompose [] = ""
    recompose ((blob, name) : xs) =
      blob <> maybe "" _rgbWriter (name >>= flip lookup digest) <> recompose xs
  in
    fmap recompose (parse parseConfig "" text)
