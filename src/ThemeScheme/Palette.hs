{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# OPTIONS_GHC -Wno-missing-fields #-}
module ThemeScheme.Palette where

import           Control.Lens                   ( makeLenses )
import qualified Data.Text                     as T
import           Data.Word                      ( Word32 )
import           ThemeScheme.Color

default (T.Text)

-- |A base color with two sets of lighter and darker shades.
data Shades a = Shades
  { _baseShade   :: a
  , _lightShades :: [a]
  , _darkShades  :: [a]
  }
  deriving (Show, Eq)

$(makeLenses ''Shades)

instance Functor Shades where
  fmap f Shades {..} =
    Shades (f _baseShade) (fmap f _lightShades) (fmap f _darkShades)

-- |Generate a set of shades by performing 'lighten' and 'darken' using the
--given @range@ and @count@ for each.
shades :: ColorSpace a => Double -> Word32 -> a -> Shades a
shades range count c = Shades c (lights range count c) (darks range count c)

-- |Generate a set of shades by performing 'lightsRamp' and 'darksRamp' using
--the given @step@ and @count@ for each.
shadesRamp :: ColorSpace a => Double -> Word32 -> a -> Shades a
shadesRamp step count color =
  Shades color (lightsRamp step count color) (darksRamp step count color)

-- |Get a list of each item in a 'Shades' where each element from the
--'lightShades' and 'darkShades' lists have "l" or "d" appended to the
-- name, respectively, followed by their list index.
--
-- >>> map fst $ shadesDigest "red" $ shades 0.9 3 (RGB 1 0 0)
-- ["red","redl0","redl1","redl2","redd0","redd1","redd2"]
shadesDigest :: T.Text -> Shades a -> [(T.Text, a)]
shadesDigest name Shades {..} =
  let digest label = zipWith (\i a -> (name <> label <> T.pack (show i), a))
                             ([0 ..] :: [Integer])
  in  (name, _baseShade) : digest "l" _lightShades <> digest "d" _darkShades

-- |A 'Swatch' provides a base set of 'Shades' and alternate 'Shades' which
--are meant to be more stand-out versions of the primary shades.
data Swatch a = Swatch
  { _primarySwatch   :: Shades a
  , _alternateSwatch :: Shades a
  }
  deriving (Show, Eq)

$(makeLenses ''Swatch)

-- |Get a list of each item in a 'Swatch' where each element from the
--'alternateSwatch' will have "A" appended to its name.
--
-- >>>map fst $ swatchDigest "red" $ Swatch (shades 0.9 2 (RGB 1 0 0)) (shades 0.9 2 (RGB 1 0.3 0.3))
-- ["red","redl0","redl1","redd0","redd1","redA","redAl0","redAl1","redAd0","redAd1"]
swatchDigest :: T.Text -> Swatch a -> [(T.Text, a)]
swatchDigest name Swatch {..} =
  shadesDigest name _primarySwatch
    <> shadesDigest (name <> "A") _alternateSwatch

instance Functor Swatch where
  fmap f Swatch {..} = Swatch (fmap f _primarySwatch) (fmap f _alternateSwatch)

-- |Provides a way to write functions that generate 'FullPalette's from
--smaller, easier to write 'Palette's.
class Palette f where
  -- |Generate the 'FullPalette' expansion of the 'Palette'.
  expandPalette :: ColorSpace a => f a -> FullPalette a

-- |The core of ThemeScheme, 'FullPalette' provides an interpretation of the
--color wheel featuring primary, secondary, and tertiary colors, and how to
--apply them to common UI elements.
--
--The colors are laid out in the order they appear in the color wheel starting
--at red and ending at pink. This means that each color can be thought of as a
--mixture of the color directly above and below it.
--
--The UI element 'Swatch'es are how the 'FullPalette' is meant to be used in
--practice.
data FullPalette a = FullPalette
  { _paletteName :: T.Text
  , _reds        :: Shades a
  , _oranges     :: Shades a
  , _yellows     :: Shades a
  , _limes       :: Shades a
  , _greens      :: Shades a
  , _aquamarines :: Shades a
  , _cyans       :: Shades a
  , _azures      :: Shades a
  , _blues       :: Shades a
  , _violets     :: Shades a
  , _magentas    :: Shades a
  , _pinks       :: Shades a
  , _blacks      :: Shades a
  , _grays       :: Shades a
  , _whites      :: Shades a
  , _background  :: Swatch a
  , _foreground  :: Swatch a
  , _border      :: Swatch a
  , _separator   :: Swatch a
  , _selected    :: Swatch a
  , _active      :: Swatch a
  , _inactive    :: Swatch a
  , _link        :: Swatch a
  , _visited     :: Swatch a
  , _negative    :: Swatch a
  , _neutral     :: Swatch a
  , _positive    :: Swatch a
  , _focus       :: Swatch a
  , _unfocus     :: Swatch a
  }
  deriving (Show, Eq)

$(makeLenses ''FullPalette)

instance Functor FullPalette where
  fmap f FullPalette {..} = FullPalette { _paletteName = _paletteName
                                        , _reds        = fmap f _reds
                                        , _oranges     = fmap f _oranges
                                        , _yellows     = fmap f _yellows
                                        , _limes       = fmap f _limes
                                        , _greens      = fmap f _greens
                                        , _aquamarines = fmap f _aquamarines
                                        , _cyans       = fmap f _cyans
                                        , _azures      = fmap f _azures
                                        , _blues       = fmap f _blues
                                        , _violets     = fmap f _violets
                                        , _magentas    = fmap f _magentas
                                        , _pinks       = fmap f _pinks
                                        , _blacks      = fmap f _blacks
                                        , _grays       = fmap f _grays
                                        , _whites      = fmap f _whites
                                        , _background  = fmap f _background
                                        , _foreground  = fmap f _foreground
                                        , _border      = fmap f _border
                                        , _separator   = fmap f _separator
                                        , _selected    = fmap f _selected
                                        , _active      = fmap f _active
                                        , _inactive    = fmap f _inactive
                                        , _link        = fmap f _link
                                        , _visited     = fmap f _visited
                                        , _negative    = fmap f _negative
                                        , _neutral     = fmap f _neutral
                                        , _positive    = fmap f _positive
                                        , _focus       = fmap f _focus
                                        , _unfocus     = fmap f _unfocus
                                        }

-- |Get a list of all of the items in the 'FullPalette' record with their names.
fullPaletteDigest :: FullPalette a -> [(T.Text, a)]
fullPaletteDigest FullPalette {..} =
  shadesDigest "red" _reds
    <> shadesDigest "orange"     _oranges
    <> shadesDigest "yellow"     _yellows
    <> shadesDigest "lime"       _limes
    <> shadesDigest "green"      _greens
    <> shadesDigest "aquamarine" _aquamarines
    <> shadesDigest "cyan"       _cyans
    <> shadesDigest "azure"      _azures
    <> shadesDigest "blue"       _blues
    <> shadesDigest "violet"     _violets
    <> shadesDigest "magenta"    _magentas
    <> shadesDigest "pink"       _pinks
    <> shadesDigest "black"      _blacks
    <> shadesDigest "gray"       _grays
    <> shadesDigest "white"      _whites
    <> swatchDigest "background" _background
    <> swatchDigest "foreground" _foreground
    <> swatchDigest "border"     _border
    <> swatchDigest "separator"  _separator
    <> swatchDigest "selected"   _selected
    <> swatchDigest "active"     _active
    <> swatchDigest "inactive"   _inactive
    <> swatchDigest "link"       _link
    <> swatchDigest "visited"    _visited
    <> swatchDigest "negative"   _negative
    <> swatchDigest "neutral"    _neutral
    <> swatchDigest "positive"   _positive
    <> swatchDigest "focus"      _focus
    <> swatchDigest "unfocus"    _unfocus

-- |Creates a readable list of all of the items in the 'FullPalatte' record
--with hex values as the colors.
fullPaletteToHex :: FullPalette RGB -> T.Text
fullPaletteToHex t =
  let digest = fullPaletteDigest t
      n      = maximum [ T.length name | (name, _) <- digest ] + 1
      padR s n | diff <= 0 = s
               | otherwise = s <> T.replicate diff " "
        where diff = n - T.length s
  in  T.init
        . T.concat
        $ [ padR name n <> "= " <> toHex (convert color) <> "\n"
          | (name, color) <- digest
          ]

-- |Creates a JSON object with a field for each item in the 'FullPalette' with
--hex colors.
fullPaletteToJson :: FullPalette RGB -> T.Text
fullPaletteToJson t =
  "{\"name\":\""
    <> _paletteName t
    <> "\","
    <> T.intercalate
         ","
         (map (\(name, c) -> "\"" <> name <> "\":\"" <> toHex c <> "\"")
              (fullPaletteDigest t)
         )
    <> "}"

instance Palette FullPalette where
  expandPalette = id

-- |A 'Palette' which generates sensible shades for given colors and applies
--them to the 'Swatch'es in a standard manner for dark themes.
data SimpleDarkPalette a = SimpleDarkPalette
  { _darkName       :: T.Text
  , _darkRed        :: a
  , _darkOrange     :: a
  , _darkYellow     :: a
  , _darkLime       :: a
  , _darkGreen      :: a
  , _darkAquamarine :: a
  , _darkCyan       :: a
  , _darkAzure      :: a
  , _darkBlue       :: a
  , _darkViolet     :: a
  , _darkMagenta    :: a
  , _darkPink       :: a
  , _darkBlack      :: a
  , _darkGray       :: a
  , _darkWhite      :: a
  }
  deriving (Show, Eq)

$(makeLenses ''SimpleDarkPalette)

instance Palette SimpleDarkPalette where
  expandPalette SimpleDarkPalette {..} =
    let
      color  = shades 0.9 3
      ground = shades 0.5 4
      stage1 = FullPalette { _paletteName = _darkName
                           , _reds        = color _darkRed
                           , _oranges     = color _darkOrange
                           , _yellows     = color _darkYellow
                           , _limes       = color _darkLime
                           , _greens      = color _darkGreen
                           , _aquamarines = color _darkAquamarine
                           , _cyans       = color _darkCyan
                           , _azures      = color _darkAzure
                           , _blues       = color _darkBlue
                           , _violets     = color _darkViolet
                           , _magentas    = color _darkMagenta
                           , _pinks       = color _darkPink
                           , _blacks      = ground _darkBlack
                           , _grays       = color _darkGray
                           , _whites      = ground _darkWhite
                           }
      stage2 p@FullPalette {..} = p
        { _background = Swatch
                          _blacks
                          (fmap (flip (blend' 0.1) (_baseShade _blues)) _blacks)
        , _foreground = Swatch
                          _whites
                          (fmap (flip (blend' 0.1) (_baseShade _blues)) _whites)
        , _separator  = Swatch _cyans _aquamarines
        , _selected   = Swatch (fmap (darken 0.1) _cyans)
                               (fmap (darken 0.1) _magentas)
        , _active     = Swatch _azures _violets
        , _inactive   = Swatch _grays _grays
        , _link       = Swatch _blues _magentas
        , _visited    = Swatch _violets _grays
        , _negative   = Swatch _reds _pinks
        , _neutral    = Swatch _oranges _yellows
        , _positive   = Swatch _greens _limes
        , _focus      = Swatch (fmap (darken 0.1) _cyans)
                               (fmap (darken 0.1) _magentas)
        }
      stage3 p@FullPalette {..} = p { _border  = fmap (lighten 0.1) _background
                                    , _unfocus = fmap (darken 0.4) _focus
                                    }
    in
      stage3 $ stage2 stage1

instance Functor SimpleDarkPalette where
  fmap f SimpleDarkPalette {..} = SimpleDarkPalette
    { _darkRed        = f _darkRed
    , _darkOrange     = f _darkOrange
    , _darkYellow     = f _darkYellow
    , _darkLime       = f _darkLime
    , _darkGreen      = f _darkGreen
    , _darkAquamarine = f _darkAquamarine
    , _darkCyan       = f _darkCyan
    , _darkAzure      = f _darkAzure
    , _darkBlue       = f _darkBlue
    , _darkViolet     = f _darkViolet
    , _darkMagenta    = f _darkMagenta
    , _darkPink       = f _darkPink
    , _darkBlack      = f _darkBlack
    , _darkGray       = f _darkGray
    , _darkWhite      = f _darkWhite
    }

-- |A 'Palette' which generates sensible shades for given colors and applies
--them to the 'Swatch'es in a standard manner for light themes.
data SimpleLightPalette a = SimpleLightPalette
  { _lightName       :: T.Text
  , _lightRed        :: a
  , _lightOrange     :: a
  , _lightYellow     :: a
  , _lightLime       :: a
  , _lightGreen      :: a
  , _lightAquamarine :: a
  , _lightCyan       :: a
  , _lightAzure      :: a
  , _lightBlue       :: a
  , _lightViolet     :: a
  , _lightMagenta    :: a
  , _lightPink       :: a
  , _lightBlack      :: a
  , _lightGray       :: a
  , _lightWhite      :: a
  }
  deriving (Show, Eq)

$(makeLenses ''SimpleLightPalette)

instance Palette SimpleLightPalette where
  expandPalette SimpleLightPalette {..} =
    let
      color  = shades 0.7 3
      ground = shadesRamp 0.06 4
      stage1 = FullPalette { _paletteName = _lightName
                           , _reds        = color _lightRed
                           , _oranges     = color _lightOrange
                           , _yellows     = color _lightYellow
                           , _limes       = color _lightLime
                           , _greens      = color _lightGreen
                           , _aquamarines = color _lightAquamarine
                           , _cyans       = color _lightCyan
                           , _azures      = color _lightAzure
                           , _blues       = color _lightBlue
                           , _violets     = color _lightViolet
                           , _magentas    = color _lightMagenta
                           , _pinks       = color _lightPink
                           , _blacks      = ground _lightBlack
                           , _grays       = color _lightGray
                           , _whites      = ground _lightWhite
                           }
      stage2 p@FullPalette {..} = p
        { _background = Swatch
                          _whites
                          (fmap (flip (blend' 0.1) (_baseShade _blues)) _whites)
        , _foreground = Swatch
                          _blacks
                          (fmap (flip (blend' 0.1) (_baseShade _blues)) _blacks)
        , _separator  = Swatch _cyans _aquamarines
        , _selected   = Swatch (fmap (lighten 0.1) _cyans)
                               (fmap (lighten 0.1) _magentas)
        , _active     = Swatch _azures _violets
        , _inactive   = Swatch _grays _grays
        , _link       = Swatch _blues _magentas
        , _visited    = Swatch _violets _grays
        , _negative   = Swatch _reds _pinks
        , _neutral    = Swatch _oranges _yellows
        , _positive   = Swatch _greens _limes
        , _focus      = Swatch (fmap (lighten 0.1) _cyans)
                               (fmap (lighten 0.1) _magentas)
        }
      stage3 p@FullPalette {..} = p { _border  = fmap (darken 0.1) _background
                                    , _unfocus = fmap (darken 0.4) _focus
                                    }
    in
      stage3 $ stage2 stage1

instance Functor SimpleLightPalette where
  fmap f SimpleLightPalette {..} = SimpleLightPalette
    { _lightRed        = f _lightRed
    , _lightOrange     = f _lightOrange
    , _lightYellow     = f _lightYellow
    , _lightLime       = f _lightLime
    , _lightGreen      = f _lightGreen
    , _lightAquamarine = f _lightAquamarine
    , _lightCyan       = f _lightCyan
    , _lightAzure      = f _lightAzure
    , _lightBlue       = f _lightBlue
    , _lightViolet     = f _lightViolet
    , _lightMagenta    = f _lightMagenta
    , _lightPink       = f _lightPink
    , _lightBlack      = f _lightBlack
    , _lightGray       = f _lightGray
    , _lightWhite      = f _lightWhite
    }
