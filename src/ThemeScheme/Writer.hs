{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
module ThemeScheme.Writer where

import qualified Data.Text                     as T
import           ThemeScheme.Color

default (T.Text)

-- | Sorrounds the text with double quotes.
doubleQuote :: T.Text -> T.Text
doubleQuote t = "\"" <> t <> "\""

-- | Composes 'toHex' with 'doubleQuote'.
toHexQuote :: RGB -> T.Text
toHexQuote = doubleQuote . toHex

-- | Formats the RGB as R,G,B.
--
-- >>>toRGBList (RGB 1 0.3 0.2)
-- "255,76,51"
toRGBList :: RGB -> T.Text
toRGBList rgb = case (fromRGB rgb :: RGB24) of
  (RGB24 r g b) -> T.pack $ show r <> "," <> show g <> "," <> show b

-- | Formats the RGB as the CSS rgb funcionts rgb(R,G,B)
toCSSRGBList :: RGB -> T.Text
toCSSRGBList rgb = "rgb(" <> toRGBList rgb <> ")"
