{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# OPTIONS_GHC -Wno-missing-fields #-}
-- |A collection of common color schemes found in text editors.
module ThemeScheme.Resources.Palettes.Common where

import           Control.Lens.Operators
import qualified Data.Text                     as T
import           ThemeScheme.Color
import           ThemeScheme.Palette

default (T.Text)

doomOne :: FullPalette RGB
doomOne =
  let stage1 = SimpleDarkPalette { _darkName       = "DoomOne"
                                 , _darkRed        = fromHexUnsafe "#ff6c6b"
                                 , _darkOrange     = fromHexUnsafe "#da8548"
                                 , _darkYellow     = fromHexUnsafe "#ECBE7B"
                                 , _darkGreen      = fromHexUnsafe "#98be65"
                                 , _darkAquamarine = fromHexUnsafe "#4db5bd"
                                 , _darkCyan       = fromHexUnsafe "#46D9FF"
                                 , _darkBlue       = fromHexUnsafe "#51afef"
                                 , _darkViolet     = fromHexUnsafe "#a9a1e1"
                                 , _darkMagenta    = fromHexUnsafe "#c678dd"
                                 , _darkBlack      = fromHexUnsafe "#282c34"
                                 , _darkGray       = fromHexUnsafe "#5B6268"
                                 , _darkWhite      = fromHexUnsafe "#bbc2cf"
                                 }
      stage2 f@SimpleDarkPalette {..} = expandPalette $ f
        { _darkLime  = blend' 0.5 _darkYellow _darkGreen
        , _darkAzure = blend' 0.5 _darkCyan _darkBlue
        , _darkPink  = blend' 0.5 _darkMagenta _darkRed
        }
      stage3 f@FullPalette {..} = f { _selected = darken 0.2 <$> _selected }
  in  stage3 $ stage2 stage1

nord :: FullPalette RGB
nord =
  let stage1 = SimpleDarkPalette { _darkName       = "Nord"
                                 , _darkRed        = fromHexUnsafe "#BF616A"
                                 , _darkOrange     = fromHexUnsafe "#D08770"
                                 , _darkYellow     = fromHexUnsafe "#EBCB8B"
                                 , _darkGreen      = fromHexUnsafe "#A3BE8C"
                                 , _darkAquamarine = fromHexUnsafe "#8FBCBB"
                                 , _darkCyan       = fromHexUnsafe "#88C0D0"
                                 , _darkBlue       = fromHexUnsafe "#81A1C1"
                                 , _darkViolet     = fromHexUnsafe "#5D80AE"
                                 , _darkMagenta    = fromHexUnsafe "#B48EAD"
                                 , _darkBlack      = fromHexUnsafe "#2E3440"
                                 , _darkGray       = fromHexUnsafe "#4C566A"
                                 , _darkWhite      = fromHexUnsafe "#ECEFF4"
                                 }
      stage2 f@SimpleDarkPalette {..} = expandPalette $ f
        { _darkLime  = blend' 0.5 _darkYellow _darkGreen
        , _darkAzure = blend' 0.5 _darkCyan _darkBlue
        , _darkPink  = blend' 0.5 _darkMagenta _darkRed
        }
  in  stage2 stage1

nordLight :: FullPalette RGB
nordLight =
  let stage1 = SimpleLightPalette
        { _lightName       = "NordLight"
        , _lightRed        = fromHexUnsafe "#99324B"
        , _lightOrange     = fromHexUnsafe "#AC4426"
        , _lightYellow     = fromHexUnsafe "#9A7500"
        , _lightGreen      = fromHexUnsafe "#4F894C"
        , _lightAquamarine = fromHexUnsafe "#29838D"
        , _lightCyan       = fromHexUnsafe "#398EAC"
        , _lightBlue       = fromHexUnsafe "#3B6EA8"
        , _lightViolet     = fromHexUnsafe "#842879"
        , _lightMagenta    = fromHexUnsafe "#97365B"
        , _lightBlack      = fromHexUnsafe "#3B4252"
        , _lightGray       = darken 0.2 $ fromHexUnsafe "#AEBACF"
        , _lightWhite      = fromHexUnsafe "#E5E9F0"
        }
      stage2 f@SimpleLightPalette {..} = expandPalette $ f
        { _lightLime  = blend' 0.5 _lightYellow _lightGreen
        , _lightAzure = blend' 0.5 _lightCyan _lightBlue
        , _lightPink  = blend' 0.5 _lightMagenta _lightRed
        }
  in  stage2 stage1

darkPlus :: FullPalette RGB
darkPlus =
  let
    stage1 = SimpleDarkPalette { _darkName       = "Dark+"
                               , _darkRed        = fromHexUnsafe "#D16969"
                               , _darkOrange     = fromHexUnsafe "#DB8E73"
                               , _darkYellow     = fromHexUnsafe "#D9DAA2"
                               , _darkLime       = fromHexUnsafe "#B5CEA8"
                               , _darkGreen      = fromHexUnsafe "#579C4C"
                               , _darkAquamarine = fromHexUnsafe "#35CDAF"
                               , _darkCyan       = fromHexUnsafe "#85DDFF"
                               , _darkAzure      = fromHexUnsafe "#237AD3"
                               , _darkBlue       = fromHexUnsafe "#339CDB"
                               , _darkViolet     = fromHexUnsafe "#68217A"
                               , _darkMagenta    = fromHexUnsafe "#C586C0"
                               , _darkBlack      = fromHexUnsafe "#1e1e1e"
                               , _darkGray       = fromHexUnsafe "#37474F"
                               , _darkWhite      = fromHexUnsafe "#d4d4d4"
                               }
    stage2 f@SimpleDarkPalette {..} =
      expandPalette $ f { _darkPink = blend' 0.5 _darkMagenta _darkRed }
    stage3 f@FullPalette {..} = f { _selected = Swatch _azures _cyans
                                  , _active   = Swatch _aquamarines _blues
                                  }
  in
    stage3 $ stage2 stage1

monokaiPro :: FullPalette RGB
monokaiPro =
  let stage1 = SimpleDarkPalette { _darkName    = "MonokaiPro"
                                 , _darkRed     = fromHexUnsafe "#CC6666"
                                 , _darkOrange  = fromHexUnsafe "#FC9867"
                                 , _darkYellow  = fromHexUnsafe "#FFD866"
                                 , _darkGreen   = fromHexUnsafe "#A9DC76"
                                 , _darkCyan    = fromHexUnsafe "#78DCE8"
                                 , _darkAzure   = fromHexUnsafe "#78DCE8"
                                 , _darkBlue    = fromHexUnsafe "#78DCE8"
                                 , _darkViolet  = fromHexUnsafe "#AB9DF2"
                                 , _darkMagenta = fromHexUnsafe "#FF6188"
                                 , _darkBlack   = fromHexUnsafe "#2D2A2E"
                                 , _darkGray    = fromHexUnsafe "#727072"
                                 , _darkWhite   = fromHexUnsafe "#FCFCFA"
                                 }
      stage2 f@SimpleDarkPalette {..} = expandPalette $ f
        { _darkLime       = blend' 0.5 _darkYellow _darkGreen
        , _darkAquamarine = blend' 0.5 _darkGreen _darkCyan
        , _darkPink       = blend' 0.5 _darkMagenta _darkRed
        }
  in  stage2 stage1

material :: FullPalette RGB
material =
  let stage1 = SimpleDarkPalette { _darkName       = "Material"
                                 , _darkRed        = fromHexUnsafe "#ff5370"
                                 , _darkOrange     = fromHexUnsafe "#f78c6c"
                                 , _darkYellow     = fromHexUnsafe "#ffcb6b"
                                 , _darkGreen      = fromHexUnsafe "#c3e88d"
                                 , _darkAquamarine = fromHexUnsafe "#44b9b1"
                                 , _darkCyan       = fromHexUnsafe "#89DDFF"
                                 , _darkBlue       = fromHexUnsafe "#82aaff"
                                 , _darkViolet     = fromHexUnsafe "#bb80b3"
                                 , _darkMagenta    = fromHexUnsafe "#c792ea"
                                 , _darkBlack      = fromHexUnsafe "#263238"
                                 , _darkGray       = fromHexUnsafe "#556369"
                                 , _darkWhite      = fromHexUnsafe "#EEFFFF"
                                 }
      stage2 f@SimpleDarkPalette {..} = expandPalette $ f
        { _darkLime  = blend' 0.5 _darkYellow _darkGreen
        , _darkAzure = blend' 0.5 _darkCyan _darkBlue
        , _darkPink  = blend' 0.5 _darkMagenta _darkRed
        }
  in  stage2 stage1

solarizedDark :: FullPalette RGB
solarizedDark =
  let stage1 = SimpleDarkPalette { _darkName       = "SolarizedDark"
                                 , _darkRed        = fromHexUnsafe "#dc322f"
                                 , _darkOrange     = fromHexUnsafe "#cb4b16"
                                 , _darkYellow     = fromHexUnsafe "#b58900"
                                 , _darkGreen      = fromHexUnsafe "#859900"
                                 , _darkAquamarine = fromHexUnsafe "#35a69c"
                                 , _darkCyan       = fromHexUnsafe "#2aa198"
                                 , _darkBlue       = fromHexUnsafe "#268bd2"
                                 , _darkViolet     = fromHexUnsafe "#6c71c4"
                                 , _darkMagenta    = fromHexUnsafe "#d33682"
                                 , _darkBlack      = fromHexUnsafe "#002b36"
                                 , _darkGray       = fromHexUnsafe "#405A61"
                                 , _darkWhite      = fromHexUnsafe "#839496"
                                 }
      stage2 f@SimpleDarkPalette {..} = expandPalette $ f
        { _darkLime  = blend' 0.5 _darkYellow _darkGreen
        , _darkAzure = blend' 0.5 _darkCyan _darkBlue
        , _darkPink  = blend' 0.5 _darkMagenta _darkRed
        }
  in  stage2 stage1

solarizedLight :: FullPalette RGB
solarizedLight =
  let stage1 = SimpleLightPalette { _lightName       = "SolarizedLight"
                                  , _lightRed        = fromHexUnsafe "#dc322f"
                                  , _lightOrange     = fromHexUnsafe "#cb4b16"
                                  , _lightYellow     = fromHexUnsafe "#b58900"
                                  , _lightGreen      = fromHexUnsafe "#859900"
                                  , _lightAquamarine = fromHexUnsafe "#35a69c"
                                  , _lightCyan       = fromHexUnsafe "#2aa198"
                                  , _lightBlue       = fromHexUnsafe "#268bd2"
                                  , _lightViolet     = fromHexUnsafe "#6c71c4"
                                  , _lightMagenta    = fromHexUnsafe "#d33682"
                                  , _lightBlack      = fromHexUnsafe "#556b72"
                                  , _lightGray       = fromHexUnsafe "#96A7A9"
                                  , _lightWhite      = fromHexUnsafe "#FDF6E3"
                                  }
      stage2 f@SimpleLightPalette {..} = expandPalette $ f
        { _lightLime  = blend' 0.5 _lightYellow _lightGreen
        , _lightAzure = blend' 0.5 _lightCyan _lightBlue
        , _lightPink  = blend' 0.5 _lightMagenta _lightRed
        }
  in  stage2 stage1

gruvboxLight :: FullPalette RGB
gruvboxLight =
  let stage1 = SimpleLightPalette { _lightName   = "GruvboxLight"
                                  , _lightRed    = fromHexUnsafe "#9d0006"
                                  , _lightOrange = fromHexUnsafe "#af3a03"
                                  , _lightYellow = fromHexUnsafe "#b57614"
                                  , _lightGreen  = fromHexUnsafe "#79740e"
                                  , _lightCyan   = fromHexUnsafe "#427b58"
                                  , _lightBlue   = fromHexUnsafe "#076678"
                                  , _lightViolet = fromHexUnsafe "#8f3f71"
                                  , _lightWhite  = fromHexUnsafe "#fbf1c7"
                                  , _lightGray   = fromHexUnsafe "#7c6f64"
                                  , _lightBlack  = fromHexUnsafe "#282828"
                                  }
      stage2 f@SimpleLightPalette {..} = expandPalette $ f
        { _lightLime       = blend' 0.5 _lightYellow _lightGreen
        , _lightAquamarine = blend' 0.5 _lightGreen _lightCyan
        , _lightAzure      = blend' 0.5 _lightCyan _lightBlue
        , _lightMagenta    = blend' 0.3 _lightViolet _lightRed
        , _lightPink       = blend' 0.3 _lightRed _lightViolet
        }
      stage3 f@FullPalette {..} = f
        { _background = let mod =
                              fmap (value (-3.2) . saturation 3.6 . hue (-7.9))
                        in  _background &~ do
                              primarySwatch . lightShades %= mod
                              primarySwatch . darkShades %= mod
        }
  in  stage3 $ stage2 stage1

gruvbox :: FullPalette RGB
gruvbox =
  let stage1 = SimpleDarkPalette { _darkName   = "Gruvbox"
                                 , _darkRed    = fromHexUnsafe "#fb4934"
                                 , _darkOrange = fromHexUnsafe "#fe8019"
                                 , _darkYellow = fromHexUnsafe "#fabd2f"
                                 , _darkGreen  = fromHexUnsafe "#b8bb26"
                                 , _darkCyan   = fromHexUnsafe "#8ec07c"
                                 , _darkBlue   = fromHexUnsafe "#83a598"
                                 , _darkViolet = fromHexUnsafe "#d3869b"
                                 , _darkBlack  = fromHexUnsafe "#282828"
                                 , _darkGray   = fromHexUnsafe "#928374"
                                 , _darkWhite  = fromHexUnsafe "#fbf1c7"
                                 }
      stage2 f@SimpleDarkPalette {..} = expandPalette $ f
        { _darkLime       = blend' 0.5 _darkYellow _darkGreen
        , _darkAquamarine = blend' 0.5 _darkGreen _darkCyan
        , _darkAzure      = blend' 0.5 _darkCyan _darkBlue
        , _darkMagenta    = blend' 0.3 _darkViolet _darkRed
        , _darkPink       = blend' 0.3 _darkRed _darkViolet
        }
      stage3 f@FullPalette {..} = f
        { _background = let mod = fmap (value 9.8 . saturation 14.5 . hue 7.5)
                        in  _background &~ do
                              primarySwatch . lightShades %= mod
                              primarySwatch . darkShades %= mod
        }
  in  stage3 $ stage2 stage1

draculaBlack :: FullPalette RGB
draculaBlack = dracula &~ do
  paletteName .= "DraculaBlack"
  background . primarySwatch . baseShade %= value (-20)
  background . primarySwatch . lightShades %= fmap (value (-3))
  background . primarySwatch . darkShades %= fmap (value (-3))

dracula :: FullPalette RGB
dracula =
  let stage1 = SimpleDarkPalette { _darkName    = "Dracula"
                                 , _darkRed     = fromHexUnsafe "#ff5555"
                                 , _darkOrange  = fromHexUnsafe "#ffb86c"
                                 , _darkYellow  = fromHexUnsafe "#f1fa8c"
                                 , _darkGreen   = fromHexUnsafe "#50fa7b"
                                 , _darkCyan    = fromHexUnsafe "#8be9fd"
                                 , _darkBlue    = fromHexUnsafe "#0088cc"
                                 , _darkViolet  = fromHexUnsafe "#bd93f9"
                                 , _darkMagenta = fromHexUnsafe "#ff79c6"
                                 , _darkBlack   = fromHexUnsafe "#282a36"
                                 , _darkGray    = fromHexUnsafe "#44475a"
                                 , _darkWhite   = fromHexUnsafe "#f8f8f2"
                                 }
      stage2 f@SimpleDarkPalette {..} = expandPalette $ f
        { _darkLime       = blend' 0.5 _darkYellow _darkGreen
        , _darkAquamarine = blend' 0.5 _darkGreen _darkCyan
        , _darkAzure      = blend' 0.5 _darkCyan _darkBlue
        , _darkPink       = blend' 0.5 _darkMagenta _darkRed
        }
      stage3 f@FullPalette {..} = f
        { _selected = Swatch (fmap (darken 0.1) _violets) _magentas
        , _focus    = Swatch (fmap (darken 0.1) _violets) _blues
        , _inactive = Swatch (shades 0.9 3 $ fromHexUnsafe "#6272a4") _grays
        , _link     = Swatch _violets _magentas
        , _visited  = Swatch _grays _grays
        , _negative = Swatch _reds (fmap (lighten 0.2) _reds)
        , _neutral  = Swatch _oranges (fmap (lighten 0.2) _oranges)
        , _positive = Swatch _greens (fmap (lighten 0.2) _greens)
        }
      stage4 f@FullPalette {..} = f { _unfocus = fmap (darken 0.3) _focus }
  in  stage4 $ stage3 $ stage2 stage1
