{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# OPTIONS_GHC -Wno-missing-fields #-}
-- |A set of themes for Emacs from <https://github.com/ogdenwebb/emacs-kaolin-themes>
module ThemeScheme.Resources.Palettes.Kaolin where

import           Control.Lens.Operators
import qualified Data.Text                     as T
import           ThemeScheme.Color
import           ThemeScheme.Palette

default (T.Text)

kaolinAurora :: FullPalette RGB
kaolinAurora = expandPalette $ SimpleDarkPalette
  { _darkName       = "KaolinAurora"
  , _darkRed        = fromHexUnsafe "#e55c7a"
  , _darkOrange     = fromHexUnsafe "#e67417"
  , _darkYellow     = fromHexUnsafe "#f5c791"
  , _darkLime       = fromHexUnsafe "#68f385"
  , _darkGreen      = fromHexUnsafe "#31E183"
  , _darkAquamarine = fromHexUnsafe "#63E8C1"
  , _darkCyan       = fromHexUnsafe "#49bdb0"
  , _darkAzure      = fromHexUnsafe "#62D2DB"
  , _darkBlue       = fromHexUnsafe "#4ca6e8"
  , _darkViolet     = fromHexUnsafe "#8B48CF"
  , _darkMagenta    = fromHexUnsafe "#9d81ba"
  , _darkPink       = fromHexUnsafe "#CE8EC8"
  , _darkBlack      = fromHexUnsafe "#14191e"
  , _darkGray       = fromHexUnsafe "#454459"
  , _darkWhite      = fromHexUnsafe "#e6e6e8"
  }

kaolinDark :: FullPalette RGB
kaolinDark =
  let stage1 = SimpleDarkPalette { _darkName       = "KaolinDark"
                                 , _darkRed        = fromHexUnsafe "#D6224D"
                                 , _darkOrange     = fromHexUnsafe "#dbac66"
                                 , _darkYellow     = fromHexUnsafe "#f5c791"
                                 , _darkLime       = fromHexUnsafe "#6dd797"
                                 , _darkGreen      = fromHexUnsafe "#6fb593"
                                 , _darkAquamarine = fromHexUnsafe "#49bdb0"
                                 , _darkCyan       = fromHexUnsafe "#6bd9db"
                                 , _darkBlue       = fromHexUnsafe "#41b0f3"
                                 , _darkViolet     = fromHexUnsafe "#9d81ba"
                                 , _darkMagenta    = fromHexUnsafe "#cea2ca"
                                 , _darkPink       = fromHexUnsafe "#d24b83"
                                 , _darkBlack      = fromHexUnsafe "#18181B"
                                 , _darkGray       = fromHexUnsafe "#6E6884"
                                 , _darkWhite      = fromHexUnsafe "#e4e4e8"
                                 }
      stage2 f@SimpleDarkPalette {..} =
        expandPalette $ f { _darkAzure = blend' 0.5 _darkCyan _darkBlue }
      stage3 f@FullPalette {..} = f
        { _background = let mod = fmap (hue (-42) . saturation 15 . value 7.9)
                        in  _background &~ do
                              primarySwatch . lightShades %= mod
                              primarySwatch . darkShades %= mod
        , _selected   = _selected & primarySwatch .~ shades
                          0.9
                          3
                          (value 3 $ saturation' 3 $ fromHexUnsafe "#2E403B")
        }
      stage4 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage4 $ stage3 $ stage2 stage1

kaolinGalaxy :: FullPalette RGB
kaolinGalaxy =
  let stage1 = expandPalette $ SimpleDarkPalette
        { _darkName       = "KaolinGalaxy"
        , _darkRed        = fromHexUnsafe "#ef6787"
        , _darkOrange     = fromHexUnsafe "#f5c791"
        , _darkYellow     = fromHexUnsafe "#eed891"
        , _darkLime       = fromHexUnsafe "#65E6A7"
        , _darkGreen      = fromHexUnsafe "#6dd797"
        , _darkAquamarine = fromHexUnsafe "#49bdb0"
        , _darkCyan       = fromHexUnsafe "#6bd9db"
        , _darkAzure      = fromHexUnsafe "#0bc9cf"
        , _darkBlue       = fromHexUnsafe "#41b0f3"
        , _darkViolet     = fromHexUnsafe "#9d81ba"
        , _darkMagenta    = fromHexUnsafe "#cea2ca"
        , _darkPink       = fromHexUnsafe "#e55c7a"
        , _darkBlack      = fromHexUnsafe "#212026"
        , _darkGray       = fromHexUnsafe "#615B75"
        , _darkWhite      = fromHexUnsafe "#e4e4e8"
        }
      stage2 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (value 10 $ saturation' 10 $ fromHexUnsafe "#402E33")
        }
      stage3 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage3 $ stage2 stage1

kaolinOcean :: FullPalette RGB
kaolinOcean =
  let stage1 = SimpleDarkPalette { _darkName       = "KaolinOcean"
                                 , _darkRed        = fromHexUnsafe "#e84c58"
                                 , _darkOrange     = fromHexUnsafe "#dbac66"
                                 , _darkYellow     = fromHexUnsafe "#eed891"
                                 , _darkLime       = fromHexUnsafe "#7CF083"
                                 , _darkGreen      = fromHexUnsafe "#35BF88"
                                 , _darkAquamarine = fromHexUnsafe "#0ed49b"
                                 , _darkCyan       = fromHexUnsafe "#6bd9db"
                                 , _darkBlue       = fromHexUnsafe "#4ca6e8"
                                 , _darkViolet     = fromHexUnsafe "#9587DD"
                                 , _darkMagenta    = fromHexUnsafe "#c79af4"
                                 , _darkPink       = fromHexUnsafe "#d24b83"
                                 , _darkBlack      = fromHexUnsafe "#14141e"
                                 , _darkGray       = fromHexUnsafe "#545c5e"
                                 , _darkWhite      = fromHexUnsafe "#e4e4e8"
                                 }
      stage2 f@SimpleDarkPalette {..} =
        expandPalette $ f { _darkAzure = blend' 0.5 _darkCyan _darkBlue }
      stage3 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (value 3 $ saturation' 3 $ fromHexUnsafe "#2E403B")
        }
      stage4 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage4 $ stage3 $ stage2 stage1

kaolinShiva :: FullPalette RGB
kaolinShiva =
  let stage1 = expandPalette $ SimpleDarkPalette
        { _darkName       = "KaolinShiva"
        , _darkRed        = fromHexUnsafe "#e84c58"
        , _darkOrange     = fromHexUnsafe "#f3c91f"
        , _darkYellow     = fromHexUnsafe "#eed891"
        , _darkLime       = fromHexUnsafe "#eeeb28"
        , _darkGreen      = fromHexUnsafe "#91f368"
        , _darkAquamarine = fromHexUnsafe "#0ed49b"
        , _darkCyan       = fromHexUnsafe "#6bd9db"
        , _darkAzure      = fromHexUnsafe "#A1DDD7"
        , _darkBlue       = fromHexUnsafe "#41b0f3"
        , _darkViolet     = fromHexUnsafe "#9d81ba"
        , _darkMagenta    = fromHexUnsafe "#cea2ca"
        , _darkPink       = fromHexUnsafe "#feb193"
        , _darkBlack      = fromHexUnsafe "#2a2028"
        , _darkGray       = fromHexUnsafe "#84686E"
        , _darkWhite      = fromHexUnsafe "#fcefe6"
        }
      stage2 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (value 15 $ saturation' 15 $ fromHexUnsafe "#392E40")
        }
      stage3 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage3 $ stage2 stage1

kaolinTemple :: FullPalette RGB
kaolinTemple =
  let stage1 = SimpleDarkPalette { _darkName       = "KaolinTemple"
                                 , _darkRed        = fromHexUnsafe "#ef6787"
                                 , _darkOrange     = fromHexUnsafe "#f3c91f"
                                 , _darkYellow     = fromHexUnsafe "#eed891"
                                 , _darkLime       = fromHexUnsafe "#b9c791"
                                 , _darkGreen      = fromHexUnsafe "#74B09A"
                                 , _darkAquamarine = fromHexUnsafe "#4FA8A3"
                                 , _darkCyan       = fromHexUnsafe "#49bdb0"
                                 , _darkAzure      = fromHexUnsafe "#68DFE8"
                                 , _darkBlue       = fromHexUnsafe "#4EB8CA"
                                 , _darkMagenta    = fromHexUnsafe "#fbaed2"
                                 , _darkPink       = fromHexUnsafe "#DC4473"
                                 , _darkBlack      = fromHexUnsafe "#2B2B2F"
                                 , _darkGray       = fromHexUnsafe "#697375"
                                 , _darkWhite      = fromHexUnsafe "#EEDCC1"
                                 }
      stage2 f@SimpleDarkPalette {..} =
        expandPalette $ f { _darkViolet = blend' 0.5 _darkBlue _darkMagenta }
      stage3 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (value 10 $ saturation' 10 $ fromHexUnsafe "#402E33")
        }
      stage4 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage4 $ stage3 $ stage2 stage1

kaolinEclipse :: FullPalette RGB
kaolinEclipse =
  let stage1 = expandPalette $ SimpleDarkPalette
        { _darkName       = "KaolinEclipse"
        , _darkRed        = fromHexUnsafe "#e84c58"
        , _darkOrange     = fromHexUnsafe "#f3c91f"
        , _darkYellow     = fromHexUnsafe "#f5c791"
        , _darkLime       = fromHexUnsafe "#35BF88"
        , _darkGreen      = fromHexUnsafe "#0D9C94"
        , _darkAquamarine = fromHexUnsafe "#80bcb6"
        , _darkCyan       = fromHexUnsafe "#6bd9db"
        , _darkAzure      = fromHexUnsafe "#41b0f3"
        , _darkBlue       = fromHexUnsafe "#738FD7"
        , _darkViolet     = fromHexUnsafe "#9587DD"
        , _darkMagenta    = fromHexUnsafe "#C68EDE"
        , _darkPink       = fromHexUnsafe "#fbaed2"
        , _darkBlack      = fromHexUnsafe "#2B1D2B"
        , _darkGray       = fromHexUnsafe "#7A6884"
        , _darkWhite      = fromHexUnsafe "#F0EBE7"
        }
      stage2 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (value 3 $ saturation' 3 $ fromHexUnsafe "#2E403B")
        }
      stage3 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage3 $ stage2 stage1

kaolinBlossom :: FullPalette RGB
kaolinBlossom =
  let stage1 = expandPalette $ SimpleDarkPalette
        { _darkName       = "KaolinBlossom"
        , _darkRed        = fromHexUnsafe "#e84c58"
        , _darkOrange     = fromHexUnsafe "#f3c91f"
        , _darkYellow     = fromHexUnsafe "#eae46a"
        , _darkLime       = fromHexUnsafe "#c7ee53"
        , _darkGreen      = fromHexUnsafe "#65E6A7"
        , _darkAquamarine = fromHexUnsafe "#53E6B5"
        , _darkCyan       = fromHexUnsafe "#6bd9db"
        , _darkAzure      = fromHexUnsafe "#8ee6d6"
        , _darkBlue       = fromHexUnsafe "#4ca6e8"
        , _darkViolet     = fromHexUnsafe "#ab98b5"
        , _darkMagenta    = fromHexUnsafe "#cea2ca"
        , _darkPink       = fromHexUnsafe "#BA5B75"
        , _darkBlack      = fromHexUnsafe "#2E2025"
        , _darkGray       = fromHexUnsafe "#6B4B53"
        , _darkWhite      = fromHexUnsafe "#EEEED3"
        }
      stage2 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (value 3 $ saturation' 3 $ fromHexUnsafe "#453038")
        }
      stage3 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage3 $ stage2 stage1

kaolinBubblegum :: FullPalette RGB
kaolinBubblegum =
  let stage1 = SimpleDarkPalette { _darkName    = "KaolinBubblegum"
                                 , _darkRed     = fromHexUnsafe "#e55c7a"
                                 , _darkOrange  = fromHexUnsafe "#dbac66"
                                 , _darkYellow  = fromHexUnsafe "#eed891"
                                 , _darkLime    = fromHexUnsafe "#31E183"
                                 , _darkGreen   = fromHexUnsafe "#63E8C1"
                                 , _darkCyan    = fromHexUnsafe "#62D2DB"
                                 , _darkAzure   = fromHexUnsafe "#526AF3"
                                 , _darkBlue    = fromHexUnsafe "#41b0f3"
                                 , _darkViolet  = fromHexUnsafe "#9587DD"
                                 , _darkMagenta = fromHexUnsafe "#c79af4"
                                 , _darkPink    = fromHexUnsafe "#CE8EC8"
                                 , _darkBlack   = fromHexUnsafe "#14171e"
                                 , _darkGray    = fromHexUnsafe "#454459"
                                 , _darkWhite   = fromHexUnsafe "#e6e6e8"
                                 }
      stage2 f@SimpleDarkPalette {..} = expandPalette
        $ f { _darkAquamarine = blend' 0.5 _darkGreen _darkCyan }
      stage3 f@FullPalette {..} = f { _selected = darken 0.1 <$> _selected
                                    , _focus    = darken 0.1 <$> _focus
                                    , _unfocus  = darken 0.1 <$> _unfocus
                                    }
  in  stage3 $ stage2 stage1

kaolinMonoDark :: FullPalette RGB
kaolinMonoDark =
  let stage1 = expandPalette $ SimpleDarkPalette
        { _darkName       = "KaolinMonoDark"
        , _darkRed        = fromHexUnsafe "#cd5c60"
        , _darkOrange     = fromHexUnsafe "#dbac66"
        , _darkYellow     = fromHexUnsafe "#dbac66"
        , _darkLime       = fromHexUnsafe "#47ba99"
        , _darkGreen      = fromHexUnsafe "#40826d"
        , _darkAquamarine = fromHexUnsafe "#90aea1"
        , _darkCyan       = fromHexUnsafe "#80bcb6"
        , _darkAzure      = fromHexUnsafe "#53859d"
        , _darkBlue       = fromHexUnsafe "#53859d"
        , _darkViolet     = fromHexUnsafe "#ab98b5"
        , _darkMagenta    = fromHexUnsafe "#ab98b5"
        , _darkPink       = fromHexUnsafe "#cd5c60"
        , _darkBlack      = fromHexUnsafe "#141e1b"
        , _darkGray       = fromHexUnsafe "#41544B"
        , _darkWhite      = fromHexUnsafe "#cfd2cb"
        }
      stage2 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (value 8 $ saturation' 8 $ fromHexUnsafe "#25352D")
        , _link     = Swatch _limes _limes
        , _visited  = Swatch _greens _greens
        }
      stage3 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage3 $ stage2 stage1

kaolinValleyDark :: FullPalette RGB
kaolinValleyDark =
  let stage1 = SimpleDarkPalette { _darkName       = "KaolinValleyDark"
                                 , _darkRed        = fromHexUnsafe "#e84c58"
                                 , _darkOrange     = fromHexUnsafe "#f3c91f"
                                 , _darkYellow     = fromHexUnsafe "#eed891"
                                 , _darkGreen      = fromHexUnsafe "#91f368"
                                 , _darkAquamarine = fromHexUnsafe "#35BF88"
                                 , _darkCyan       = fromHexUnsafe "#6bd9db"
                                 , _darkAzure      = fromHexUnsafe "#7ED7E6"
                                 , _darkBlue       = fromHexUnsafe "#41b0f3"
                                 , _darkViolet     = fromHexUnsafe "#9587DD"
                                 , _darkMagenta    = fromHexUnsafe "#cea2ca"
                                 , _darkPink       = fromHexUnsafe "#ef6787"
                                 , _darkBlack      = fromHexUnsafe "#262221"
                                 , _darkGray       = fromHexUnsafe "#635C4A"
                                 , _darkWhite      = fromHexUnsafe "#e6e6e8"
                                 }
      stage2 f@SimpleDarkPalette {..} =
        expandPalette $ f { _darkLime = blend' 0.5 _darkOrange _darkGreen }
      stage3 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (value 5 $ saturation' 5 $ fromHexUnsafe "#402e2e")
        , _active   = darken 0.1 <$> _active
        }
      stage4 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage4 $ stage3 $ stage2 stage1

kaolinLight :: FullPalette RGB
kaolinLight =
  let stage1 = expandPalette $ SimpleLightPalette
        { _lightName       = "KaolinLight"
        , _lightRed        = fromHexUnsafe "#ef6787"
        , _lightOrange     = fromHexUnsafe "#C5882C"
        , _lightYellow     = fromHexUnsafe "#E36B3F"
        , _lightLime       = fromHexUnsafe "#39854C"
        , _lightGreen      = fromHexUnsafe "#13665F"
        , _lightAquamarine = fromHexUnsafe "#47ba99"
        , _lightCyan       = fromHexUnsafe "#6facb3"
        , _lightAzure      = fromHexUnsafe "#48a9a9"
        , _lightBlue       = fromHexUnsafe "#3B84CC"
        , _lightViolet     = fromHexUnsafe "#6D46E3"
        , _lightMagenta    = fromHexUnsafe "#a9779c"
        , _lightPink       = fromHexUnsafe "#D6224D"
        , _lightBlack      = fromHexUnsafe "#383e3f"
        , _lightGray       = fromHexUnsafe "#7c878a"
        , _lightWhite      = fromHexUnsafe "#EDEEEB"
        }
      stage2 f@FullPalette {..} = f
        { _selected   = _selected & primarySwatch .~ shades
                          0.9
                          3
                          (saturation' 15 $ fromHexUnsafe "#D3E4F0")
        , _background = let mod =
                              fmap (hue (-60) . saturation (-1) . value 7.8)
                        in  _background &~ do
                              primarySwatch . lightShades %= mod
                              primarySwatch . darkShades %= mod
        }
      stage3 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage3 $ stage2 stage1

kaolinBreeze :: FullPalette RGB
kaolinBreeze =
  let stage1 = SimpleLightPalette { _lightName    = "KaolinBreeze"
                                  , _lightRed     = fromHexUnsafe "#cd5c60"
                                  , _lightOrange  = fromHexUnsafe "#ee7042"
                                  , _lightYellow  = fromHexUnsafe "#b87e3c"
                                  , _lightGreen   = fromHexUnsafe "#39855f"
                                  , _lightCyan    = fromHexUnsafe "#48a9a9"
                                  , _lightAzure   = fromHexUnsafe "#4F9CB8"
                                  , _lightBlue    = fromHexUnsafe "#2683b5"
                                  , _lightViolet  = fromHexUnsafe "#605DB3"
                                  , _lightMagenta = fromHexUnsafe "#845A84"
                                  , _lightPink    = fromHexUnsafe "#ef6787"
                                  , _lightBlack   = fromHexUnsafe "#383e3f"
                                  , _lightGray    = fromHexUnsafe "#7D8468"
                                  , _lightWhite   = fromHexUnsafe "#EBE8E4"
                                  }
      stage2 f@SimpleLightPalette {..} = expandPalette $ f
        { _lightLime       = blend' 0.5 _lightOrange _lightGreen
        , _lightAquamarine = blend' 0.5 _lightGreen _lightCyan
        }
      stage3 f@FullPalette {..} = f
        { _selected   = _selected & primarySwatch .~ shades
                          0.9
                          3
                          (darken 0.2 $ fromHexUnsafe "#C9C2BD")
        , _background = let mod = fmap $ lighten 0.1
                        in  _background &~ do
                              primarySwatch . lightShades %= mod
                              primarySwatch . darkShades %= mod
        }
  in  stage3 $ stage2 stage1

kaolinMonoLight :: FullPalette RGB
kaolinMonoLight =
  let stage1 = expandPalette $ SimpleLightPalette
        { _lightName       = "KaolinMonoLight"
        , _lightRed        = fromHexUnsafe "#ef6787"
        , _lightOrange     = fromHexUnsafe "#C5882C"
        , _lightYellow     = fromHexUnsafe "#d1832e"
        , _lightLime       = fromHexUnsafe "#5E7801"
        , _lightGreen      = fromHexUnsafe "#317A56"
        , _lightAquamarine = fromHexUnsafe "#39854C"
        , _lightCyan       = fromHexUnsafe "#03878C"
        , _lightAzure      = fromHexUnsafe "#40826d"
        , _lightBlue       = fromHexUnsafe "#1D6B64"
        , _lightViolet     = fromHexUnsafe "#9d81ba"
        , _lightMagenta    = fromHexUnsafe "#D16BD1"
        , _lightPink       = fromHexUnsafe "#D6224D"
        , _lightBlack      = fromHexUnsafe "#4E4757"
        , _lightGray       = fromHexUnsafe "#7D8468"
        , _lightWhite      = fromHexUnsafe "#F1F3E9"
        }
      stage2 f@FullPalette {..} = f
        { _selected   = lighten 0.2 <$> Swatch _greens _limes
        , _link       = Swatch _limes _limes
        , _visited    = Swatch _greens _greens
        , _background = let mod = fmap $ lighten 0.1
                        in  _background &~ do
                              primarySwatch . lightShades %= mod
                              primarySwatch . darkShades %= mod
        }
      stage3 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage3 $ stage2 stage1

kaolinValleyLight :: FullPalette RGB
kaolinValleyLight =
  let stage1 = SimpleLightPalette { _lightName    = "KaolinValleyLight"
                                  , _lightRed     = fromHexUnsafe "#e84c58"
                                  , _lightOrange  = fromHexUnsafe "#d1832e"
                                  , _lightYellow  = fromHexUnsafe "#F84B1B"
                                  , _lightLime    = fromHexUnsafe "#18803A"
                                  , _lightGreen   = fromHexUnsafe "#0D7A75"
                                  , _lightCyan    = fromHexUnsafe "#0bc9cf"
                                  , _lightAzure   = fromHexUnsafe "#526AF3"
                                  , _lightBlue    = fromHexUnsafe "#0F79BF"
                                  , _lightViolet  = fromHexUnsafe "#9d81ba"
                                  , _lightMagenta = fromHexUnsafe "#cf44ac"
                                  , _lightPink    = fromHexUnsafe "#D6224D"
                                  , _lightBlack   = fromHexUnsafe "#5E5854"
                                  , _lightGray    = fromHexUnsafe "#AEA19E"
                                  , _lightWhite   = fromHexUnsafe "#FAF2E9"
                                  }
      stage2 f@SimpleLightPalette {..} = expandPalette
        $ f { _lightAquamarine = blend' 0.5 _lightGreen _lightCyan }
      stage3 f@FullPalette {..} = f
        { _selected = _selected & primarySwatch .~ shades
                        0.9
                        3
                        (saturation' 15 $ fromHexUnsafe "#D3EED3")
        }
      stage4 f@FullPalette {..} =
        f { _focus = _selected, _unfocus = darken 0.3 <$> _selected }
  in  stage4 $ stage3 $ stage2 stage1
