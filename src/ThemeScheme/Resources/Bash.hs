{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
module ThemeScheme.Resources.Bash where

import qualified Data.Text                     as T
import           GHC.IO.Exception
import           System.Process
import           ThemeScheme.Color
import           ThemeScheme.Palette
import           ThemeScheme.Writer

default (T.Text)

-- | Creates a list of bash variable exports for each color in the 'FullPalette'.
createBashPalette :: FullPalette RGB -> [T.Text]
createBashPalette palette =
  map (\(n, c) -> "export " <> n <> "=" <> toHexQuote c)
    $ fullPaletteDigest palette

-- | Attempts to kill the program with the specified name iff the @pidof@ it is
--not null. Will continue to try and kill the program until the @pidof@ is null.
--
--The command issued is as follows:
-- @while pidof name > /dev/null; do killall name; sleep 0.1; done@.
safeKill :: String -> IO ExitCode
safeKill name =
  system
    $  "while pidof "
    <> name
    <> " > /dev/null; do killall "
    <> name
    <> "; sleep 0.1; done"

