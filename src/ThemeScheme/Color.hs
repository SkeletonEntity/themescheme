{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}
module ThemeScheme.Color
  ( ColorSpace(..)
  , RGB(..)
  , RGB24(..)
  , toHex
  , fromHex
  , fromHexUnsafe
  , RYB(..)
  , convert
  , value
  , lighten
  , lights
  , lightsRamp
  , darken
  , darks
  , darksRamp
  , complementary
  , analogous
  , analogousEasy
  , splitComplementary
  , triad
  , tetrad
  , hue'
  , saturation'
  , blend'
  , blends
  , blends'
  , blendsRamp
  , blendsRamp'
  , complementary'
  , analogous'
  , analogousEasy'
  , splitComplementary'
  , triad'
  , tetrad'
  , HSV(..)
  ) where

import           Control.Lens                   ( Each(each) )
import           Control.Lens.Operators
import           Control.Monad                  ( when )
import           Control.Monad.ST               ( runST )
import           Data.Fixed                     ( mod' )
import           Data.STRef                     ( modifySTRef
                                                , newSTRef
                                                , readSTRef
                                                )
import qualified Data.Text                     as T
import           Data.Word                      ( Word32
                                                , Word8
                                                )
import           GHC.Stack                      ( HasCallStack )
import           Numeric                        ( readHex
                                                , showHex
                                                )
import           ThemeScheme.Util
default(T.Text)

-- |Color spaces are ways to organize and represent color. Many 'ColorSpace's have
--a unique color wheel, which means colors considered to be complementary,
--analogous, triadic, etc. are different from other 'ColorSpace's. For instance,
--in 'RYB' red and green are complementary, and in 'RGB' red and cyan are complementary.
--Blending also changes depending on the layout of the color wheel.
--
--'RGB' is considered the "base" 'ColorSpace' and all other 'ColorSpace's can
--convert to and from 'RGB'. Conversion to 'RGB' must be perceptually
--lossless, and result in no extra difference upon further conversions to and
--from 'RGB'. I.E the mapping between 'RGB' and the other 'ColorSpace' must be
--nearly 1-to-1.
--
--The intended way to use a 'ColorSpace' is to start from an 'RGB' value, convert
--to another 'ColorSpace' to perform color wheel dependent functions before
--converting back to 'RGB' to use the value somewhere else.
class ColorSpace a where
  -- |A perfect or nearly perfect 1-to-1 mapping to 'RGB' color space.
  toRGB :: a -> RGB

  -- |A perfect or nearly perfect 1-to-1 mapping from 'RGB' color space.
  fromRGB :: RGB -> a

  -- |Rotate the color by the angle in degrees and return the new color.
  --The results of rotation differ for each color space.
  --
  --RGB color space
  -- >>>toHex $ hue 180 (RGB 1 0 0)
  -- "#00ffff"
  --
  --RYB color space
  -- >>>toHex $ convert $ hue 180 (RYB 1 0 0)
  -- "#00ff00"
  hue :: Double -> a -> a

  -- |Modify the saturation of the color by a value. This value is clamped to
  --[0,100].
  --
  -- >>>map (toHex . convert) [RYB 0.7 0.7 0.4, saturation 50 $ RYB 0.7 0.7 0.4]
  -- ["#b28c66","#b2600d"]
  saturation :: Double -> a -> a

  -- |Blend two colors by the amount alpha. Alpha must be clamped to [0, 1].
  --Each color space blends differently.
  --
  --RGB color space
  -- >>>map (\x -> toHex $ blend x (RGB 1 0 0) (RGB 0 1 0)) [0,0.25..1]
  -- ["#ff0000","#bf4000","#808000","#40bf00","#00ff00"]
  --
  --RYB color space
  -- >>>map (\x -> toHex $ convert $ blend x (RYB 1 0 0) (RYB 0 1 1)) [0,0.25..1]
  -- ["#ff0000","#bf4040","#808080","#40bf40","#00ff00"]
  --
  --HSV color space
  -- >>>map (\x -> toHex $ convert $ blend x (HSV 0 100 100) (HSV 120 100 100)) [0,0.25..1]
  -- ["#ff0000","#ff8000","#ffff00","#80ff00","#00ff00"]
  blend :: Double -> a -> a -> a

-- |The [RGB](https://en.wikipedia.org/wiki/RGB_color_model) (Red, Green, Blue) color space represented with 'Double's.
--Using 'Double' instead of 'Word8' vastly improves the accuracy of 'ColorSpace'
--conversions. The parameters are ordered (red green blue).
data RGB = RGB Double Double Double
  deriving (Show, Eq)

blend3 :: (Ord c, Num c) => c -> (c, c, c) -> (c, c, c) -> (c, c, c)
blend3 alpha (a1, b1, c1) (a2, b2, c2) =
  let clamped = clamp (0, 1) alpha
      blendN n1 n2 = n1 * (1 - clamped) + n2 * clamped
  in  (blendN a1 a2, blendN b1 b2, blendN c1 c2)

uncurry3 :: (t1 -> t2 -> t3 -> t4) -> (t1, t2, t3) -> t4
uncurry3 f (r, g, b) = f r g b

instance ColorSpace RGB where
  toRGB   = id
  fromRGB = id
  hue d rgb = convert (hue d $ convert rgb :: HSV)
  saturation d rgb = convert (saturation d $ convert rgb :: HSV)
  blend alpha (RGB r1 g1 b1) (RGB r2 g2 b2) =
    uncurry3 RGB $ blend3 alpha (r1, g1, b1) (r2, g2, b2)

-- |'RGB' in the standard 24bit format. Meant for importing and exporting
--colors, not for doing color operations like 'blend' or 'hue'. You have
--been warned. The parameters are ordered (red green blue).
data RGB24 = RGB24 Word8 Word8 Word8
  deriving (Show, Eq)

instance ColorSpace RGB24 where
  toRGB (RGB24 r g b) =
    RGB (fromIntegral r / 255) (fromIntegral g / 255) (fromIntegral b / 255)
  fromRGB (RGB r g b) =
    RGB24 (round $ r * 255) (round $ g * 255) (round $ b * 255)
  hue d rgb = convert (hue d $ convert rgb :: HSV)
  saturation d rgb = convert (saturation d $ convert rgb :: HSV)
  blend alpha c1 c2 = fromRGB $ blend alpha (toRGB c1) (toRGB c2)

-- |Converts the 'RGB' to HTML hex.
--
-- >>>toHex $ RGB 1 0 0
-- "#ff0000"
--
-- >>>toHex $ RGB 0 1 0
-- "#00ff00"
-- >>>map (\x -> toHex $ blend x (RGB 1 0 0) (RGB 0 1 0)) [0,0.25..1]
toHex :: RGB -> T.Text
toHex (RGB r g b) = '#' `T.cons` T.concat
  (map
    ( (\hex -> if T.length hex == 1 then '0' `T.cons` hex else hex)
    . (\x -> T.pack $ showHex (round . (* 255) $ x :: Word8) "")
    )
    [r, g, b]
  )

-- |Tries to read an HTML hex value from the text. The "#" is optional.
--
-- >>>fromHex "#ff0000"
-- Right (RGB {red = 1.0, green = 0.0, blue = 0.0})
--
-- >>>fromHex "00ff88"
-- Right (RGB {red = 0.0, green = 1.0, blue = 0.5333333333333333})
fromHex :: T.Text -> Either String RGB
fromHex txt =
  let removeThorpe txt = case T.uncons txt of
        Nothing            -> Left "Hex RGB text cannot be empty."
        (Just ('#', txt')) -> Right txt'
        _                  -> Right txt
      fromList [r, g, b] = Right $ RGB (r / 255) (g / 255) (b / 255)
      fromList _         = Left "Hex RGB must have 6 digits."
  in  do
        txt' <- removeThorpe txt
        fromList $ map (fst . head . readHex . T.unpack) $ T.chunksOf 2 txt'

-- |Performs 'fromHex', but if the result is 'Left' it will throw an error.
fromHexUnsafe :: HasCallStack => T.Text -> RGB
fromHexUnsafe txt = case fromHex txt of
  Left  err -> error err
  Right c   -> c

-- |The [RYB](https://en.wikipedia.org/wiki/RYB_color_model) (Red, Yellow, Blue) color space. This color space is similar to the
--color wheel used in art and design, which makes it the preferable color space
--for finding complements, triads, tetrads, and for blending. The parameters are
--ordered (red yellow blue).
data RYB = RYB Double Double Double
  deriving (Show, Eq)

-- |Conversion algorithms from
--https://web.archive.org/web/20130525061042/www.insanit.net/tag/rgb-to-ryb/
instance ColorSpace RYB where
  toRGB (RYB r y b) = runST
    (do
      let white = minimum [r, y, b]
      ryb       <- newSTRef ((r, y, b) & each -~ white)
      (r, y, b) <- readSTRef ryb
      let maxYellow = maximum [r, y, b]
          g         = min y b
      modifySTRef ryb (\(r, y, b) -> (r, y - g, b - g))
      (r, y, b) <- readSTRef ryb
      rgb       <- newSTRef (r, g, b)
      when (b > 0 && g > 0) (modifySTRef rgb (\(r, g, b) -> (r, g * 2, b * 2)))
      modifySTRef rgb (\(r, g, b) -> (r + y, g + y, b))
      (r, g, b) <- readSTRef rgb
      let maxGreen = maximum [r, g, b]
          n        = maxYellow / maxGreen
      when (maxGreen > 0) (modifySTRef rgb (each *~ n))
      modifySTRef rgb (each +~ white)
      (r, g, b) <- readSTRef rgb
      pure $ RGB r g b
    )
  fromRGB (RGB r g b) = runST
    (do
      let white = minimum [r, g, b]
      rgb       <- newSTRef ((r, g, b) & each -~ white)
      (r, g, b) <- readSTRef rgb
      let maxGreen = maximum [r, g, b]
          y        = min r g
      modifySTRef rgb (\(r, g, b) -> (r - y, g - y, b))
      when (b > 0 && g > 0) (modifySTRef rgb (\(r, g, b) -> (r, g / 2, b / 2)))
      (r, g, b) <- readSTRef rgb
      ryb       <- newSTRef (r, y + g, b + g)
      (r, y, b) <- readSTRef ryb
      let maxYellow = maximum [r, y, b]
          n         = maxGreen / maxYellow
      when (maxYellow > 0) (modifySTRef ryb (each *~ n))
      modifySTRef ryb (each +~ white)
      (r, y, b) <- readSTRef ryb
      pure $ RYB r y b
    )
  hue d (RYB r y b) = case hue d (RGB r y b) of
    (RGB r g b) -> RYB r g b
  saturation d (RYB r y b) = case saturation d (RGB r y b) of
    (RGB r g b) -> RYB r g b
  blend alpha (RYB r1 y1 b1) (RYB r2 y2 b2) =
    uncurry3 RYB $ blend3 alpha (r1, y1, b1) (r2, y2, b2)

-- |Converts from 'ColorSpace' a to 'ColorSpace' b by passing through 'RGB'.
convert :: (ColorSpace a, ColorSpace b) => a -> b
convert = fromRGB . toRGB

-- |Modify the value, also known as lightness, of the color. A positive @delta@
--results in a lighter color, and negative a dark color. The value is clamped to
--[0,100].
--
-- >>> map toHex [RGB 0.5 0.5 0.5, value 50 $ RGB 0.5 0.5 0.5]
-- ["#808080","#ffffff"]
value :: ColorSpace a => Double -> a -> a
value delta c = case convert c :: HSV of
  (HSV h s v) -> convert $ HSV h s (clamp (0, 100) (v + delta))

-- |Blend the color @c@ with white by the amount @alpha@.
-- @alpha@ is clamped to [0, 1]
lighten :: ColorSpace a => Double -> a -> a
lighten alpha c = blend alpha c (convert $ RGB 1 1 1)

-- |Generate a set of colors lighter than @color@ within the @range@.
--
--A set of colors are generated by dividing @range@ by @count@, and creating a
--color for each step from @range / count@ to @range@.
--
-- >>>map toHex $ lights 1 4 (RGB 1 0 0)
-- ["#ff4040","#ff8080","#ffbfbf","#ffffff"]
lights :: ColorSpace a => Double -> Word32 -> a -> [a]
lights range count color =
  let start = range / fromIntegral count
  in  map (`lighten` color) [start, start * 2 .. range]

-- |Generate a set of colors progressively lighter than @color@ stepping by
--the amount @step@ for each color until the @count@ is reached.
--
-- >>>map toHex $ lightsRamp 0.25 4 (RGB 1 0 0)
-- ["#ff4040","#ff8080","#ffbfbf","#ffffff"]
lightsRamp :: ColorSpace a => Double -> Word32 -> a -> [a]
lightsRamp step count color =
  map (`lighten` color) [step, step * 2 .. step * fromIntegral count]

-- |The same as 'lighten' except @c@ is blended with black.
darken :: ColorSpace a => Double -> a -> a
darken alpha c = blend alpha c (convert $ RGB 0 0 0)

-- |The same as 'lights' except @c@ is blended with black.
--
-- >>>map toHex $ darks 1 4 (RGB 1 0 0)
-- ["#bf0000","#800000","#400000","#000000"]
darks :: ColorSpace a => Double -> Word32 -> a -> [a]
darks range count color =
  let start = range / fromIntegral count
  in  map (`darken` color) [start, start * 2 .. range]

-- |The same as 'lightsRamp' except @color@ is darkened.
--
-- >>>map toHex $ darksRamp 0.25 4 (RGB 1 0 0)
-- ["#bf0000","#800000","#400000","#000000"]
darksRamp :: ColorSpace a => Double -> Word32 -> a -> [a]
darksRamp step count color =
  map (`darken` color) [step, step * 2 .. step * fromIntegral count]

-- |Performs 'hue' but always within the 'RYB' 'ColorSpace'.
hue' :: ColorSpace a => Double -> a -> a
hue' theta c = convert $ hue theta (convert c :: RYB)

-- |Performs 'saturation' but always within the 'RYB' 'ColorSpace'.
saturation' :: ColorSpace a => Double -> a -> a
saturation' delta c = convert $ saturation delta (convert c :: RYB)

-- |Performs 'blend' but always within the 'RYB' 'ColorSpace'.
blend' :: ColorSpace a => Double -> a -> a -> a
blend' alpha c1 c2 =
  convert $ blend alpha (convert c1 :: RYB) (convert c2 :: RYB)

-- |Generate a set of colors progressively more blended within the @range@.
--
--A set of colors are generated by dividing @range@ by @count@, and creating a
--blended color for each step from @range / count@ to @range@.
--
-- >>>map toHex $ blends 1 4 (RGB 1 0 0) (RGB 0 1 0)
-- ["#bf4000","#808000","#40bf00","#00ff00"]
blends :: ColorSpace a => Double -> Word32 -> a -> a -> [a]
blends range count color1 color2 =
  let start = range / fromIntegral count
  in  map (\x -> blend x color1 color2) [start, start * 2 .. range]

-- |Preforms 'blends' but always within the 'RYB' 'ColorSpace'.
blends' :: ColorSpace a => Double -> Word32 -> a -> a -> [a]
blends' range count color1 color2 =
  let start = range / fromIntegral count
  in  map (\x -> blend' x color1 color2) [start, start * 2 .. range]

-- |Generate a set of colors progressively blended by stepping the amount @step@
--for each color until the @count@ is reached.
--
-- >>>map toHex $ blendsRamp 0.25 4 (RGB 1 0 0) (RGB 0 1 0)
-- ["#bf4000","#808000","#40bf00","#00ff00"]
blendsRamp :: ColorSpace a => Double -> Word32 -> a -> a -> [a]
blendsRamp step count color1 color2 = map
  (\x -> blend x color1 color2)
  [step, step * 2 .. step * fromIntegral count]

-- |Preforms 'blendsRamp' but always within the 'RYB' 'ColorSpace'.
blendsRamp' :: ColorSpace a => Double -> Word32 -> a -> a -> [a]
blendsRamp' step count color1 color2 = map
  (\x -> blend' x color1 color2)
  [step, step * 2 .. step * fromIntegral count]

-- |The color located 180 degrees from the given color.
--
--RGB complementary:
-- >>>toHex $ complementary (RGB 1 0 0)
-- "#00ffff"
--
--RYB complementary:
-- >>> toHex $ convert $ complementary (convert (RGB 1 0 0) :: RYB)
-- "#00ff00"
complementary :: ColorSpace a => a -> a
complementary = hue 180.0

-- |Perform 'complementary' but always within the 'RYB' 'ColorSpace'.
complementary' :: ColorSpace a => a -> a
complementary' = hue' 180.0

-- |Generate a list of colors at progressively further angles from @color@
--starting at @start@. Both positive and negative angle from @color@ are used,
--so the generated colors will be in the pattern of
--[@start@, -@start@, @start@ + delta, -(@start@ + delta), ...]
--
-- >>>map toHex $ analogous 20 20 4 (RGB 1 0 0)
-- ["#ff5500","#ff0055","#ffaa00","#ff00aa"]
--
-- >>>map toHex $ analogous 20 5 4 (RGB 1 0 0)
-- ["#ff5500","#ff0055","#ff6a00","#ff006a"]
analogous :: ColorSpace a => Double -> Double -> Word32 -> a -> [a]
analogous start delta count color =
  let f :: Word32 -> Double
      f n | even n    = fromIntegral $ n `div` 2
          | otherwise = fromIntegral $ (n - 1) `div` 2
      ns = map (\x -> (-1) ^ x * (start + delta * f x)) [0 .. count - 1]
  in  map (`hue` color) ns

-- |Perform 'analgous' but always within the 'RYB' 'ColorSpace'.
analogous' :: ColorSpace a => Double -> Double -> Word32 -> a -> [a]
analogous' start delta count color =
  map convert $ analogous start delta count (convert color :: RYB)

-- |A simplified version of 'analogous' where start and delta are equal.
--
-- >>>map toHex $ analogousEasy 20 4 (RGB 1 0 0)
-- ["#ff5500","#ff0055","#ffaa00","#ff00aa"]
analogousEasy :: ColorSpace a => Double -> Word32 -> a -> [a]
analogousEasy step = analogous step step

-- |Performs 'analogousEasy' but always within the 'RYB' 'ColorSpace'.
analogousEasy' :: ColorSpace a => Double -> Word32 -> a -> [a]
analogousEasy' step = analogous' step step

-- |Two colors analogous to the 'complementary'
--
-- >>>toHex $ complementary (RGB 1 0 0)
-- "#00ffff"
--
-- >>>map toHex $ splitComplementary (RGB 1 0 0)
-- ["#0080ff","#00ff80"]
splitComplementary :: ColorSpace a => a -> [a]
splitComplementary = analogousEasy 30 2 . complementary

-- |Performs 'splitComplementary' but always within the 'RYB' 'ColorSpace'.
splitComplementary' :: ColorSpace a => a -> [a]
splitComplementary' = analogousEasy' 30 2 . complementary'

-- |Two colors offset by 120 and 240 degrees from the given color.
--
-- >>>map toHex $ triad (RGB 1 0 0)
-- ["#00ff00","#0000ff"]
--
-- >>>map (toHex . convert) $ triad (convert (RGB 1 0 0) :: RYB)
-- ["#ffff00","#0000ff"]
triad :: ColorSpace a => a -> [a]
triad = analogous 120 0 2

-- |Performs 'triad' but always within the 'RYB' 'ColorSpace'.
triad' :: ColorSpace a => a -> [a]
triad' = analogous' 120 0 2

-- |Three colors offset by 90, 180, and 270 degrees from the given color.
--
-- >>>map toHex $ tetrad (RGB 1 0 0)
-- ["#80ff00","#8000ff","#00ffff"]
--
-- >>>map (toHex . convert) $ tetrad (convert (RGB 1 0 0) :: RYB)
-- ["#ffaa00","#8000ff","#00ff00"]
tetrad :: ColorSpace a => a -> [a]
tetrad = analogous 90 90 3

-- |Performs 'tetrad' but always within the 'RYB' 'ColorSpace'.
tetrad' :: ColorSpace a => a -> [a]
tetrad' = analogous' 90 90 3

-- | The [HSV](https://en.wikipedia.org/wiki/HSL_and_HSV) color space. 'HSV' is primarily used to implement 'hue' in the
--other 'ColorSpace's, but it can offer an interesting take on color blending.
--The parameters are ordered (hue saturation value).
data HSV = HSV Double Double Double
  deriving (Show, Eq)

instance ColorSpace HSV where
  toRGB (HSV h s v) =
    let
      c = v / 100 * (s / 100)
      x = c * (1 - abs (((h / 60) `mod'` 2) - 1))
      m = v / 100 - c
      (r', g', b')
        | h >= 0 && h < 60 = (c, x, 0)
        | h >= 60 && h < 120 = (x, c, 0)
        | h >= 120 && h < 180 = (0, c, x)
        | h >= 180 && h < 240 = (0, x, c)
        | h >= 240 && h < 300 = (x, 0, c)
        | h >= 300 && h < 360 = (c, 0, x)
        | otherwise = error
          "HSV color space hue is outside valid range of [0,360)."
      (r, g, b) = (r' + m, g' + m, b' + m)
    in
      RGB r g b
  fromRGB (RGB r g b) =
    let cmax  = maximum [r, g, b]
        cmin  = minimum [r, g, b]
        delta = cmax - cmin
        h | cmax == cmin = 0
          | cmax == r    = (60 * ((g - b) / delta) + 360) `mod'` 360
          | cmax == g    = (60 * ((b - r) / delta) + 120) `mod'` 360
          | cmax == b    = (60 * ((r - g) / delta) + 240) `mod'` 360
          | otherwise    = error "Unreachable"
        s | cmax == 0 = 0
          | otherwise = delta / cmax * 100
        v = cmax * 100
    in  HSV h s v
  hue delta (HSV h s v) = HSV ((h + delta) `mod'` 360) s v
  saturation delta (HSV h s v) = HSV h (clamp (0, 100) (s + delta)) v
  blend alpha (HSV h1 s1 v1) (HSV h2 s2 v2) =
    uncurry3 HSV $ blend3 alpha (h1, s1, v1) (h2, s2, v2)
