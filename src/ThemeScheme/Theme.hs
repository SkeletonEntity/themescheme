{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
module ThemeScheme.Theme where

import           Control.Lens.Combinators
import           Control.Lens.Operators
import           Control.Monad
import           Control.Monad.Cont             ( liftIO )
import           Data.Binary.Put
import qualified Data.ByteString.Lazy          as B
import qualified Data.ByteString.Lazy.Char8    as B8
import           Data.List                      ( find )
import qualified Data.Text                     as T
import qualified Data.Text.Encoding            as T
import           Katip
import           Text.Printf
import           ThemeScheme.Color
import           ThemeScheme.Config
import           ThemeScheme.Logger
import           ThemeScheme.Palette
import           ThemeScheme.Util

default (T.Text)

data Theme = Theme
  { _systemConfigs           :: [SystemConfig]
  , _palettes                :: [FullPalette RGB]
  , _defaultSystemConfigName :: T.Text
  , _defaultPaletteName      :: T.Text
  , _verbosity               :: Severity
  , _stopSystemConfig        :: Bool
  , _startSystemConfig       :: Bool
  , _save                    :: Bool
  , _jsonMessage             :: Bool
  , _errorMsg                :: Maybe String
  }

$(makeLenses ''Theme)

showError :: Theme -> String -> Theme
showError theme str = theme & errorMsg ?~ str

defaultTheme :: Theme
defaultTheme = Theme [] [] "" "" ErrorS False False True False Nothing

-- | Finds a 'SystemConfig' in '_systemConfigs' with a '_systemConfigName' equal
--to '_defaultSystemConfigName' and a 'FullPalette' from '_palettes' with
--a '_paletteName' equal to '_defaultPaletteName' then applies that 'SystemConfig'
--with that 'FullPalette'.
applyTheme :: Theme -> Logger IO ()
applyTheme Theme {..} =
  let
    systemConfig =
      find ((== _defaultSystemConfigName) . _systemConfigName) _systemConfigs
    palette = find ((== _defaultPaletteName) . _paletteName) _palettes
    tup     = (,) <$> palette <*> systemConfig
  in
    do
      maybe
        ($(logTM) ErrorS $ ls' $ printf "Could not find %s in systemConfigs"
                                        _defaultSystemConfigName
        )
        (const (pure ()))
        systemConfig
      maybe
        ($(logTM) ErrorS $ ls' $ printf "Could not find %s in palettes"
                                        _defaultPaletteName
        )
        (const (pure ()))
        palette
      if _startSystemConfig || _stopSystemConfig || _jsonMessage
        then do
          when _stopSystemConfig  (maybe (pure ()) (uncurry stopSystem) tup)
          when _startSystemConfig (maybe (pure ()) (uncurry startSystem) tup)
          when
            _jsonMessage
            (maybe
              (pure ())
              (\(p, _) -> do
                let encoded = B.fromStrict $ T.encodeUtf8 $ fullPaletteToJson p
                    len     = fromIntegral $ B.length encoded
                liftIO $ B8.putStr $ runPut (putWord32host len) <> encoded
              )
              tup
            )
        else maybe (pure ()) (uncurry applySystemConfig) tup
