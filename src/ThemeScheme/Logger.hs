{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module ThemeScheme.Logger where

import           Control.Lens
import           Control.Monad.Reader
import           Katip

data LogState = LogState
  { _logNamespace :: Namespace
  , _logContext   :: LogContexts
  , _logEnv       :: LogEnv
  }

$(makeLenses ''LogState)

newtype Logger m a = Logger
  { unLogger :: ReaderT LogState m a
  }
  deriving (MonadReader LogState, Functor, Applicative, Monad, MonadIO, MonadTrans)

instance (MonadIO m) => Katip (Logger m) where
  getLogEnv = view logEnv
  localLogEnv f (Logger m) = Logger (local (over logEnv f) m)

instance (MonadIO m) => KatipContext (Logger m) where
  getKatipContext = view logContext
  localKatipContext f (Logger m) = Logger (local (over logContext f) m)
  getKatipNamespace = view logNamespace
  localKatipNamespace f (Logger m) = Logger (local (over logNamespace f) m)

runLogger :: LogState -> Logger m a -> m a
runLogger s f = runReaderT (unLogger f) s
