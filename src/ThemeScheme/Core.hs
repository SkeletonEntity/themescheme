{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE RecordWildCards #-}
module ThemeScheme.Core
  ( themescheme
  ) where

import qualified Config.Dyre                   as Dyre
import           Control.Conditional            ( ifM )
import           Control.Exception
import           Control.Lens.Combinators
import           Control.Lens.Operators
import           Control.Monad                  ( when )
import           Control.Monad.Cont             ( liftIO )
import           Data.Char                      ( toLower )
import           Data.Maybe                     ( fromMaybe
                                                , isNothing
                                                )
import qualified Data.Text                     as T
import           Data.Text.Internal.Builder     ( fromString
                                                , fromText
                                                )
import           Data.Time
import           Katip
import           Katip.Core                     ( intercalateNs )
import           Katip.Scribes.Handle
import           Language.Haskell.TH
import           System.Console.GetOpt
import           System.Directory               ( createDirectoryIfMissing
                                                , doesFileExist
                                                )
import           System.Environment
import           System.Environment.XDG.BaseDir ( getUserCacheDir )
import           System.FilePath
import           System.IO
import           Text.Printf                    ( printf )
import           ThemeScheme.Config
import           ThemeScheme.Logger
import           ThemeScheme.Palette
import           ThemeScheme.Theme
import           ThemeScheme.Util

default (T.Text)

themescheme :: Theme -> IO ()
themescheme = Dyre.wrapMain $ (Dyre.newParams "ThemeScheme" realMain showError)
  { Dyre.statusOut = const (pure ())
  }

realMain :: Theme -> IO ()
realMain theme = do
  args         <- getArgs
  theme'       <- processArgs theme args
  handleScribe <- mkHandleScribeWithFormatter
    myFormat
    ColorIfTerminal
    stdout
    (permitItem (theme' ^. verbosity))
    V2
  let mkLogEnv =
        registerScribe "stdout" handleScribe defaultScribeSettings
          =<< initLogEnv "ThemeScheme" "production"
  bracket mkLogEnv closeScribes $ \le -> do
    let s = LogState mempty mempty le
    runLogger s $ do
      $(logTM) InfoS "ThemeScheme started"
      $(logTM) DebugS
        $  logStr
        $  "With args ["
        <> T.intercalate "," (map ((\t -> "\'" <> t <> "\'") . T.pack) args)
        <> "]"
      when
        (theme' ^. save)
        (do
          path <- liftIO getSavePath
          $(logTM) InfoS $ ls' $ printf "saving set config and palette to %s" path
          liftIO $ createDirectoryIfMissing True path
          liftIO $ writeFile (path </> "save") $ show
            (theme' ^. defaultSystemConfigName, theme' ^. defaultPaletteName)
        )
      applyTheme theme'
      pure ()

myTimeFormat :: UTCTime -> T.Text
myTimeFormat = T.pack . formatTime defaultTimeLocale "%Y-%m-%d %H:%M:%S"

myFormat :: LogItem a => ItemFormatter a
myFormat withColor verb Item {..} =
  brackets nowStr
    <> brackets (mconcat $ map fromText $ intercalateNs _itemNamespace)
    <> brackets (fromText (renderSeverity' _itemSeverity))
    <> mconcat ks
    <> maybe mempty (brackets . fromString . myLocationToString) _itemLoc
    <> fromText " "
    <> unLogStr _itemMessage
 where
  nowStr = fromText (myTimeFormat _itemTime)
  ks     = map brackets $ getKeys verb _itemPayload
  renderSeverity' severity =
    colorBySeverity withColor severity (renderSeverity severity)

myLocationToString :: Loc -> String
myLocationToString loc = loc_filename loc ++ ':' : line loc ++ ':' : char loc
 where
  line = show . fst . loc_start
  char = show . snd . loc_start

getSavePath :: IO FilePath
getSavePath = getUserCacheDir "ThemeScheme"

-- |Tries to be as lenient as possible when interpreting arguments.
--Options -p or -c will take priority over arguments, if the corresponding
--option is not passed and the '_defaultPaletteName' or '_defaultSystemConfigName'
--are empty ("") then this will attempt to fill in the empty value(s) with one
--of or both of the first two arguments. What determines which of the first two
--arguments is put where is first whether it appears in the '_systemConfigs'
--or '_palettes' and second the order, with the first argument going to
--'_defaultSystemConfigName' and second to '_defaultPaletteName'.
processArgs :: Theme -> [String] -> IO Theme
processArgs theme args = do
  (theme', args') <- themeSchemeOpts theme args
  let toMaybe t = if t == T.empty then Nothing else Just t
      select@(mc, mp) =
        (theme' ^. defaultSystemConfigName, theme' ^. defaultPaletteName)
          &  both
          %~ toMaybe
      trySystemConfigName n t =
        if n `elem` map (view systemConfigName) (t ^. systemConfigs)
          then t & defaultSystemConfigName .~ n
          else t
      tryPaletteName n t = if n `elem` map (view paletteName) (t ^. palettes)
        then t & defaultPaletteName .~ n
        else t
      tryBoth n = tryPaletteName n . trySystemConfigName n
      incorporate (c, p) t =
        let t' = t & defaultSystemConfigName .~ c & defaultPaletteName .~ p
        in  tryBoth c . tryBoth p $ t'
      loadOpts t = do
        path <- (</>) <$> getSavePath <*> pure "save"
        ifM
          (doesFileExist path)
          (do
            (c, p) <- read <$> readFile path
            pure $ incorporate
              (fromMaybe (T.pack c) mc, fromMaybe (T.pack p) mp)
              t
          )
          (pure t)
  if anyOf both isNothing select
    then case args' of
      [x] -> if allOf both isNothing select
        then fail msg
        else
          let x' = T.pack x
          in  pure $ incorporate (fromMaybe x' mc, fromMaybe x' mp) theme'
      (c : p : _) -> pure $ incorporate
        (fromMaybe (T.pack c) mc, fromMaybe (T.pack p) mp)
        theme'
      _ -> loadOpts theme'
    else loadOpts theme'
 where
  msg
    = "Please specify two arguments or the options (-c, -p) to set a theme to apply"

themeSchemeOpts :: Theme -> [String] -> IO (Theme, [String])
themeSchemeOpts theme argv = case getOpt Permute options argv of
  (o, n, [] ) -> pure (foldl (flip id) theme o, n)
  (_, _, err) -> ioError (userError (concat err <> usageInfo header options))
  where header = "Usage: themescheme [OPTIONS] args.."

options :: [OptDescr (Theme -> Theme)]
options =
  [ Option ['c']
           ["config"]
           (ReqArg parseConfigName "SYSTEM CONFIG NAME")
           "The name of the system config to apply"
  , Option ['p']
           ["palette"]
           (ReqArg parsePaletteName "PALETTE NAME")
           "The name of the palette to apply"
  , Option ['v']
           ["verbosity"]
           (ReqArg parseVerbosity "VERBOSITY")
           "An integer [0,4] or debug, info, warn, error, none"
  , Option ['r']
           ["stop"]
           (NoArg setStop)
           "Run stop on all programs in the system config."
  , Option ['s']
           ["start"]
           (NoArg setStart)
           "Run start on all programs in the system config."
  , Option ['n'] ["noSave"] (NoArg setNoSave) "Disable saving these options"
  , Option ['j']
           ["jsonMessage"]
           (NoArg setJSON)
           "Send palette as a JSON object to stdout"
  ]
 where
  parseConfigName str opts = opts & defaultSystemConfigName .~ T.pack str
  parsePaletteName str opts = opts & defaultPaletteName .~ T.pack str
  setStop opts = opts & stopSystemConfig .~ True
  setStart opts = opts & startSystemConfig .~ True
  setNoSave opts = opts & save .~ False
  setJSON opts = opts & jsonMessage .~ True & verbosity .~ EmergencyS
  parseVerbosity str opts = opts & verbosity .~ parseSeverity (map toLower str)
   where
    parseSeverity str | str `elem` ["4", "debug"]             = DebugS
                      | str `elem` ["3", "info"]              = InfoS
                      | str `elem` ["2", "warning", "warn"]   = WarningS
                      | str `elem` ["1", "error"]             = ErrorS
                      | str `elem` ["0", "emergency", "none"] = EmergencyS
                      | str == "notice"                       = NoticeS
                      | str == "critical"                     = CriticalS
                      | str == "alert"                        = AlertS
                      | otherwise = error $ "Unknown verbosity: " <> str
