module Main where

import           ThemeScheme.Core
import           ThemeScheme.Theme

main :: IO ()
main = themescheme defaultTheme
